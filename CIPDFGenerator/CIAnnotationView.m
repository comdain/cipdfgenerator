//
//  AnnotationView.m
//  CIPDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 6/8/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import "CIAnnotationView.h"


@interface CIAnnotationView ()<UIGestureRecognizerDelegate, UIScrollViewDelegate, getLastAnnotatedViewDelegate>

@property (nonatomic, weak) ILPDFView<UIScrollViewDelegate> *pdfView;
@property (nonatomic, strong) CIAnnotationZoomView *zoomView;
@property(nonatomic, readonly) ILPDFPage *currentPage;
@property (nonatomic) CGPDFPageRef page;
@property (nonatomic) CGRect frameRect;
@property (nonatomic) CGPoint pointInPDFView;
@property (nonatomic) CGRect frameToAnnotate;
@property (nonatomic, strong) NSMutableArray *annotableFrames;

@property (nonatomic, strong) NSMutableArray *zoomViews;

@property (nonatomic, strong) NSMutableArray *incrementalImages;
@property (nonatomic, strong) NSArray<PDFAnnotations *> *previousAnnotations;

@property (nonatomic, strong) UIImage *combinedImage;

@end

@implementation CIAnnotationView

- (id)initWithPDFView:(PDFView *)pdfView andpdfDocument:(ILPDFDocument *)pdfDocument andAnnottaions:(NSArray *)annotations
{
    self = [super initWithFrame:pdfView.frame];
  
    if (self)
    {
        self.pdfView = (ILPDFView<UIScrollViewDelegate> *)pdfView;
        
        self.frameRect = pdfView.frame;
      
        CGRect frameRect = pdfView.frame;
//        frameRect.size.height = pdfView.scrollView.contentSize.height;
        self.viewContainer = [[UIView alloc]initWithFrame:frameRect];
 
        [self addSubview:self.viewContainer];
        
//        self.annotableFrames = [self.pdfView pageFrames];
        self.annotableFrames = [NSMutableArray array];
        for (PDFPage *page in pdfView.visiblePages)
        {
            [self.annotableFrames addObject:[NSValue valueWithCGRect:pdfView.frame]];
        }
        
        self.zoomViews = [NSMutableArray array];
        self.annotatedDocuments = [NSMutableArray array];
        self.annotations = [NSMutableArray arrayWithArray:annotations];
        PDFAnnotations *lastAnnotation = [self.annotations lastObject];
        
        if (annotations.count != 0)
        {
            for (CIAnnotationZoomView *zoomView in lastAnnotation.allPossileZoomViews)
            {
                self.zoomView = zoomView;
                self.zoomView.annotDelegate = self;
                self.zoomView.pdfView = self.pdfView;
                [self.zoomViews addObject:self.zoomView];
               
                [self.viewContainer addSubview:self.zoomView];
            }
            for (UIImage *incrementalImage in lastAnnotation.incrementImages)
            {
                [self.incrementalImages addObject:incrementalImage];
            }
        }
        else
        {
            for (id frame in self.annotableFrames)
            {
                self.zoomView = [[CIAnnotationZoomView alloc]initWithFrame:[frame CGRectValue] andPDFView:self.pdfView andAnnotationView:self andAnnotatedImage:nil];
                self.zoomView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.5 alpha:0.2];
                self.zoomView.annotDelegate = self;
                [self.zoomViews addObject:self.zoomView];
                [self.viewContainer addSubview:self.zoomView];
            }
        }
        
        self.delegate = self;
    
//        [self setZoomScalesAndContent:pdfView];
       
    }
   
    return self;
    
}


- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.viewContainer;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([self.pdfView respondsToSelector:@selector(scrollViewDidScroll:)])
    {
        [self.pdfView.pdfView.scrollView setContentOffset:scrollView.contentOffset];
        
        [self.pdfView scrollViewDidScroll:scrollView];
    }
}


- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{    
    if ([self.pdfView respondsToSelector:@selector(scrollViewDidZoom:)])
    {
        [self.pdfView.pdfView.scrollView setContentOffset:scrollView.contentOffset];
        [self.pdfView.pdfView.scrollView setContentSize:scrollView.contentSize];
        [self.pdfView.pdfView.scrollView setZoomScale:scrollView.zoomScale];

        [self.pdfView scrollViewDidZoom:self.pdfView.pdfView.scrollView];
    }
}


-(void)doAnnotation
{
    for (CIAnnotationZoomView *zoomView in self.zoomViews)
    {
        zoomView.isErase = NO;
    }
    
    [self setScrollEnabled:NO];
}


- (void)findPage
{
    self.frameToAnnotate = [self.pdfView pageFrameAtPoint:self.pointInPDFView];
    
    [self setFrame:self.frameToAnnotate];
    [self setNeedsLayout];
    [self layoutIfNeeded];
}


-(void)undoPressed
{
    PDFAnnotations *lastAnnotation = [self.annotations lastObject];
    [lastAnnotation.zoomView.incrementalImages removeLastObject];

    [self.annotations removeLastObject];
    for (CIAnnotationZoomView *zoomview in self.zoomViews)
    {
        [zoomview setNeedsDisplay];
        [zoomview setNeedsLayout];
    }
    
    [self.annotatedDocumentDelegate updateAnnotationforQueuElement:self.annotations];
}


- (void)erasePressed
{
    [self setScrollEnabled:NO];
    for (CIAnnotationZoomView *zoomView in self.zoomViews)
    {
        [zoomView eraseAnnotation];
    }
}


#pragma mark - ZoomView Delegate

- (void)getView: (CIAnnotationZoomView *)zoomView andIncrementalImage:(NSArray *)incrementalImages andPageNumber:(NSInteger) pageNumber
{
    self.incrementalImages = [NSMutableArray array];

    for (UIImage *incrementalImage in incrementalImages)
    {
          [self.incrementalImages addObject:incrementalImage];
    }
    
    PDFAnnotations *annotation = [[PDFAnnotations alloc]initWithImage:self.incrementalImages andZoomView:zoomView andPageNumber:pageNumber andAllPossibleZoomViews:self.zoomViews];
    
    if(self.annotations == nil)
    {
        self.annotations = [NSMutableArray array];
    }
    
    [self.annotations addObject:annotation];
    
    [self.annotatedDocumentDelegate setAnnotationElement:annotation];

}


-(void)setZoomScalesAndContent : (ILPDFView *)pdfView
{
    [pdfView.pdfView.scrollView setContentOffset:
     CGPointMake(0, pdfView.pdfView.scrollView.contentInset.top) animated:YES];
 
    self.userInteractionEnabled = YES;
    [self setScrollEnabled:YES];
    self.maximumZoomScale = 5.0f;
    self.minimumZoomScale = 1.0f;
    self.zoomScale = 1.0;
    self.contentSize = pdfView.pdfView.scrollView.contentSize;
}


- (void)setStrokeColor:(UIColor *)strokeColor
{
    for (CIAnnotationZoomView *zoomView in self.zoomViews)
    {
        zoomView.strokeColor = strokeColor;
    }
}


-(void)setStrokeWidth : (NSInteger )strokeWidth
{
    for (CIAnnotationZoomView *zoomView in self.zoomViews)
    {
        zoomView.strokeWidth = strokeWidth;
    }
}

@end
