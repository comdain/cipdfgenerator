//
//  PDFAnnotations.h
//  CIPDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 16/8/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ILPDFKit.h"
#import "CIAnnotationZoomView.h"

@interface PDFAnnotations : NSObject

@property (strong,nonatomic) CIAnnotationZoomView *zoomView;
@property (strong,nonatomic)  NSArray *incrementImages;
@property (nonatomic, assign) NSInteger pageNumber;
@property (nonatomic, strong) NSArray *allPossileZoomViews;

- (instancetype)initWithImage: (NSArray *)annotationImages andZoomView: (CIAnnotationZoomView *)zoomView andPageNumber:(NSInteger )pageNumber  andAllPossibleZoomViews: (NSArray *)allZoomViews;

@end
