//
//  CIPDFSignatureViewController.m
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 25/6/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import "CIPDFSignatureViewController.h"
#import "ILPDFSignatureEditingView.h"


@interface CIPDFSignatureViewController ()
@property (weak, nonatomic) IBOutlet UIView *nameAndDateView;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet ILPDFSignatureEditingView *signatureView;
@end

@implementation CIPDFSignatureViewController
{
    ILPDFSignatureEditingView *signView ;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    self.preferredContentSize=CGSizeMake(600, 400);
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Cancel"
                                   style:UIBarButtonItemStyleDone
                                   target:self
                                   action:@selector(signatureAction:)];
    
    UIBarButtonItem *clearButton = [[UIBarButtonItem alloc]
                                    initWithTitle:@"Clear"
                                    style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(clearAction:)];
    self.navigationItem.rightBarButtonItem = cancelButton;
    self.navigationItem.leftBarButtonItem = clearButton;
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    self.dateLabel.text = [dateFormatter stringFromDate:[NSDate date]];
    self.nameTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    [self.nameTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Actions

- (IBAction)clearAction:(UIButton *)sender
{
    [self.signatureView clearSignature];
}


- (IBAction)signatureAction:(UIButton *)sender
{
    if (self.nameTextField.text != nil)
    {
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        UIImage *signImage = [self.signatureView createImageFromSignWithMaxWidth:3000 andMaxHeight:380];
        [self.delegate signedWithImage:signImage andSignatureName:self.nameTextField.text andSignatureDate:[dateFormatter stringFromDate:[NSDate date]]];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
       [self dismissViewControllerAnimated:YES completion:nil];
    }
}


- (IBAction)cancelButtonPressed:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma Mark - TextFeild method

- (void)textFieldDidChange:(UITextField *)textField
{
    if (![textField.text isEqualToString:@""])
    {
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
                                       initWithTitle:@"Done"
                                       style:UIBarButtonItemStyleDone
                                       target:self
                                       action:@selector(signatureAction:)];
        self.navigationItem.rightBarButtonItem = doneButton;
    }
    else
    {
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]
                                       initWithTitle:@"Cancel"
                                       style:UIBarButtonItemStyleDone
                                       target:self
                                       action:@selector(signatureAction:)];
        self.navigationItem.rightBarButtonItem = cancelButton;
    }
}
@end
