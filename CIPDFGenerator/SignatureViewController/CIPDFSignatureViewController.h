//
//  CIPDFSignatureViewController.h
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 25/6/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CIPDFSignatureControllerDelegate;

@interface CIPDFSignatureViewController : UIViewController
@property (nonatomic, weak)   IBOutlet id <CIPDFSignatureControllerDelegate> delegate;

@end

@protocol CIPDFSignatureControllerDelegate <NSObject>

@optional

- (void) signedWithImage:(UIImage*) signatureImage andSignatureName: (NSString *)signatureName andSignatureDate:(NSString *)signatureDate;

@end
