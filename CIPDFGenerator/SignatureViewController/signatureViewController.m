//
//  signatureViewController.m
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 25/6/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import "signatureViewController.h"
#import "CIPDFSignatureViewController.h"
#import "ILPDFSignatureController.h"

@interface signatureViewController () <UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate,CIPDFSignatureControllerDelegate, UITextFieldDelegate, UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *signatureTable;
@property (strong, nonatomic) IBOutlet  UILabel *addNewLabel;
@property (strong, nonatomic) IBOutlet UIButton *addSignatureButton;
@property (strong, nonatomic) IBOutlet UIImageView *imageForSignature;
@property (retain, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) NSMutableArray *signatureImages;
@property (strong, nonatomic) IBOutlet NSMutableDictionary *signatureInfo;
@property (strong, nonatomic)NSIndexPath *clickedButtonIndexPath;

@end

@implementation signatureViewController

- (instancetype)initWithSignature:(NSMutableArray*)signatures andProcessBlock:(void (^)(NSArray *signatures))signaturesProcessed
{
    self = [super init];
    
    if (self)
    {
        self.signatureArray = signatures;
        self.signaturesProcessed = signaturesProcessed;
    }
    
    return self;
}


- (instancetype)initWithSignatures:(NSMutableArray*)signatures
{
    self = [super init];
    
    if (self)
    {
        self.signatureArray = signatures;
    }
    
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
   
    UINavigationBar* navbar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-50, 50)];
    
    UINavigationItem* navItem = [[UINavigationItem alloc] initWithTitle:@"Add Signature"];
    
    UIBarButtonItem* doneBtn = [[UIBarButtonItem alloc]
                                initWithTitle:@"Done"
                                style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonPressed)];
    navItem.rightBarButtonItem = doneBtn;
    
    [navbar setItems:@[navItem]];
    [self.view addSubview:navbar];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed)];
    
    self.signatureTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.nameLabel.userInteractionEnabled = NO;
    
    [self.signatureTable reloadData];
    [self scrollToBottom];
    
}


#pragma mark - TableView Datasource and Delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.signatureArray.count + 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    for (UIView *subView in cell.contentView.subviews)
    {
        [subView removeFromSuperview];
    }
    
    self.addNewLabel = [[UILabel alloc] initWithFrame:CGRectMake(150, 0, 250, 50)];
    self.addNewLabel.textColor = [UIColor grayColor];
    self.addNewLabel.text = @"Click to add Signature";
   
    NSInteger totalRow = [tableView numberOfRowsInSection:indexPath.section];
    if (indexPath.row == totalRow - 1)
    {
          [cell.contentView addSubview:self.addNewLabel];
    }
    else
    {
        [self addCellContent];
        [cell.contentView addSubview:self.nameLabel];
        [cell.contentView addSubview:self.imageForSignature];
        
        self.nameLabel.text = [NSString stringWithFormat:@" %@",[[self.signatureArray objectAtIndex:indexPath.row] objectForKey:@"Name"]];
        
        self.imageForSignature.image = [[self.signatureArray objectAtIndex:indexPath.row] objectForKey:@"SigntureImage"];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger totalRow = [tableView numberOfRowsInSection:indexPath.section];
    if(indexPath.row == totalRow - 1 )
    {
        CIPDFSignatureViewController  *vc = [CIPDFSignatureViewController alloc];
        vc.delegate = self;
        
        UINavigationController *popOverNavigationController = [[UINavigationController alloc] initWithRootViewController:vc];
        popOverNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        popOverNavigationController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [self presentViewController:popOverNavigationController animated:YES completion:nil];

        [self addCellContent];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        [self.addNewLabel removeFromSuperview];

        [cell.contentView addSubview:self.nameLabel];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}


-(void)addCellContent
{
    CGRect buttonRect = CGRectMake(380, 10 , 150, 35);
    
    self.imageForSignature = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10 , 150, 35)];
    self.imageForSignature.frame = buttonRect;
    self.imageForSignature.backgroundColor = [UIColor clearColor];
    self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 10, 185, 40)];
    self.nameLabel.adjustsFontSizeToFitWidth = YES;
    self.nameLabel.textColor = [UIColor blackColor];

    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
}


#pragma mark - Navigation buttons

- (void)doneButtonPressed
{
    if (self.signaturesProcessed != nil)
    {
        self.signaturesProcessed(self.signatureArray);
    }
    
   [self dismissViewControllerAnimated:YES completion:nil];
    
}


#pragma - TextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([newString length] <= [textField.text length])
    {
        [self.addSignatureButton setEnabled:YES];
    }

    {
         [self.addSignatureButton setEnabled:YES];
    }
    
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    UITableViewCell *cell = (UITableViewCell *)[[textField superview] superview];
    UITableView *table = (UITableView *)[cell superview];
    NSIndexPath *textFieldIndexPath = [table indexPathForCell:cell];
    NSLog(@"Row %ld just finished editing with the value %@",(long)textFieldIndexPath.row,textField.text);
}


#pragma mark - Signature Controller Delegate

- (void) signedWithImage:(UIImage*) signatureImage andSignatureName: (NSString *)signatureName andSignatureDate:(NSString *)signatureDate
{
    UITableViewCell *cell = [self.signatureTable cellForRowAtIndexPath:self.clickedButtonIndexPath];
    
    self.signatureInfo = [NSMutableDictionary dictionary];
    [self.signatureInfo setObject:signatureName forKey:@"Name"];
    [self.signatureInfo setObject:signatureDate forKey:@"Date"];
    [self.signatureInfo setObject:signatureImage forKey:@"SigntureImage"];
    if (![signatureName isEqualToString:@""])
    {
        [self.signatureArray addObject:self.signatureInfo];
    }
    [cell.contentView addSubview:self.imageForSignature];
    [self.signatureTable reloadData];
    self.imageForSignature.image = signatureImage;
    [self scrollToBottom];
   
}


#pragma Mark - TableView Scroll
-(void)scrollToBottom
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSIndexPath *topPath = [NSIndexPath indexPathForRow:[self.signatureTable numberOfRowsInSection:0] - 1 inSection:0];
        [self.signatureTable scrollToRowAtIndexPath:topPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    });
    
}
@end
