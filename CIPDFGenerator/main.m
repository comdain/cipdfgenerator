//
//  main.m
//  CIPDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 9/8/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
