//
//  GGPDFViewController.m
//  CIPDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 9/7/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import "GGPDFViewController.h"

@interface GGPDFViewController ()

@end

@implementation GGPDFViewController

- (NSArray *)generateAllPDFs
{
    NSData *pdfData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:@"https://compass.comdain.com.au/sites/hseq/_layouts/15/start.aspx#/PreTaskAssessment/Forms/AllItems.aspx"]];
    NSString *resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle] resourcePath] stringByDeletingLastPathComponent]
                                                                  stringByAppendingPathComponent:@"Documents"
                                                                  ]];
    NSString *filePath = [resourceDocPath
                          stringByAppendingPathComponent:@"myPDF.pdf"];
    [pdfData writeToFile:filePath atomically:YES];

    NSURL *url = [NSURL fileURLWithPath:filePath];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];

    NSMutableArray *pdfs = [NSMutableArray array];
    NSArray *pdfArray = [NSArray arrayWithObjects: @"Test.pdf",@"CMS-0000-HS-FM-003-A Hot Works Permit.pdf",@"CMS-0000-HS-FM-007-A Working at Heights Permit.pdf",@"CMS-0000-HS-FM-015-A Lift Plan Checklist.pdf",@"CMS-0000-HS-FM-016-B Plant and Equipment Checklist.pdf",@"CMS-0000-HS-FM-017-A Excavation Permit.pdf",@"CMS-0000-HS-FM-017-B Ground Disturbance Checklist.pdf",@"CMS-0000-HS-FM-041-A Overhead Services Checklist.pdf",@"CMS-0000-HS-FM-041-D HV Power Checklist.pdf",@"CMS-3000-HS-FM-002-A Confined Space Permit (Victoria).pdf",nil];
    for (NSString* pdfName in pdfArray)
    {
        NSString *pathWithExtension = pdfName;
        NSString *pdfPath = pathWithExtension.stringByDeletingPathExtension;
                NSString *path = [[NSBundle mainBundle] pathForResource:pdfPath ofType:@"pdf"];
        if (![path isEqualToString:@""])
        {
            NSData *pdfData = [NSData dataWithContentsOfFile:path];
            PDFQueueElement *element = [[PDFQueueElement alloc]initWithPdfName:pdfName andOriginalDocumentData:pdfData];
            [pdfs addObject:element];
        }
    }
    
    return [pdfs copy];
}

@end
