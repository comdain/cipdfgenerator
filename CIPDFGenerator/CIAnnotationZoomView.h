//
//  CIAnnotController.h
//  CIPDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 7/8/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import "ILPDFKit.h"
#import <UIKit/UIKit.h>

@protocol getLastAnnotatedViewDelegate;

@interface CIAnnotationZoomView : UIView 
@property (nonatomic, strong) NSMutableArray *incrementalImages;
@property (nonatomic, strong)  NSMutableArray *viewWithIncrementalImage;
@property (nonatomic, weak) id <getLastAnnotatedViewDelegate> annotDelegate;
//@property (nonatomic, weak) ILPDFView *pdfView;
@property (nonatomic, strong) PDFView *pdfView;
@property (nonatomic, strong) UIColor *strokeColor;
@property (nonatomic, assign) NSInteger strokeWidth;
@property (nonatomic) BOOL isErase;


- (id)initWithFrame:(CGRect)frame andPDFView:(ILPDFView *)pdfView andAnnotationView: (UIView *)anotationView andAnnotatedImage: (NSArray *)lastAnnotatedImage;

- (NSInteger )findPage;
- (void)drawBitmap;
- (BOOL) getLastAnnotatedVie : (CGRect)viewFrame andView:(CIAnnotationZoomView *)view;
- (void)eraseAnnotation;

@end

@protocol getLastAnnotatedViewDelegate <NSObject>

@optional

- (void)getView: (CIAnnotationZoomView *)zoomView andIncrementalImage:(NSArray *)incrementalImages andPageNumber:(NSInteger)pageNumber;


@end

