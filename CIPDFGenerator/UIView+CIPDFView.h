//
//  UIView+CIPDFView.h
//  CIPDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 6/7/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@class ILPDFFormTextField;
@class UILayoutGuide;

@interface UIView (CIPDFView)

- (ILPDFFormTextField * _Nullable)activePDFTextField;
-(void)pinToSuperview: (UIEdgeInsets)insets;
-(void)pinToSuperview: (UIEdgeInsets)insets guide:(UILayoutGuide * _Nonnull)guide;

@end
