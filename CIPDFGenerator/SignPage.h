//
//  SignPage.h
//  CIPDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 6/12/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <PDFKit/PDFKit.h>
#import "ILPDFViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SignPage : PDFPage

@end

NS_ASSUME_NONNULL_END
