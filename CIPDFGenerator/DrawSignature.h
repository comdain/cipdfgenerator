//
//  drawSignature.h
//  CIPDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 3/12/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <PDFKit/PDFKit.h>

@interface DrawSignature : PDFAnnotation

@property (nonatomic, strong) UIImage *image;
@property (nonatomic) CGRect *drawBounds;


-(instancetype)initWithImage:(UIImage *)signatureImage bounds:(CGRect )bounds withProperties:AnyHashable withPage:(PDFPage *)page;

@end

