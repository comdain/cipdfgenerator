//
//  IPhoto.h
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 31/5/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IPhoto <NSObject>

//@property (nonatomic, strong) id<IWorkOrder> workOrder;
@property (nonatomic, strong) NSString *caption;
@property (nonatomic, strong) NSString *assetNumber;
@property (nonatomic, strong) NSString *imageType;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSNumber *sectionSortOrder;
@property (nonatomic, strong) NSMutableDictionary *exifData;
@property (nonatomic, strong) NSString *guid;
@property (nonatomic, strong) NSString *sharepointID;
@property (nonatomic, strong) NSString *takenBy;
@property (nonatomic, strong) NSString *filename;
@property (nonatomic, strong) NSData *data;

@property (nonatomic) BOOL hasBeenEditedValue;
@property (nonatomic) BOOL hasBeenDeletedValue;

- (NSString *)timestampAsString;
@end
