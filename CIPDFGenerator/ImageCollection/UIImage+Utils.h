//
//  UIImage+Utils.h
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 22/6/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Utils)

- (UIImage*)addBorder;

@end
