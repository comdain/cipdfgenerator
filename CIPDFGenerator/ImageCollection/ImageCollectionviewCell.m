//
//  ImageCollectionviewCell.m
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 30/5/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import "ImageCollectionviewCell.h"

@interface ImageCollectionviewCell ()


@end
@implementation ImageCollectionviewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (UIImageView *) imageView
{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        [self.contentView addSubview:_imageView];
    }
    return _imageView;
}

// Here we remove all the custom stuff that we added to our subclassed cell
-(void)prepareForReuse
{
    [super prepareForReuse];
    
    [self.imageView removeFromSuperview];
    self.imageView = nil;
}

- (void)longPress:(UILongPressGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateBegan)
    {
        self.mode = PhotoCellModeDelete;
    }
}


//- (void)setPhoto:(id<IPhoto>)photo
//{
//    _photo = photo;
//    
//    if (photo)
//    {
//        [self.imageView setImage:_photo.image];
//        [self.captionLabel setText:((_photo.caption == nil) ? @"" : _photo.caption)]; // [NSString stringWithFormat:@" %@", _photo.caption]]
//        [self.dateLabel setText:[self.photo timestampAsString]];
//    }
//}

- (void)setMode:(PhotoCellMode)mode
{
    BOOL showToolbar;
    
    _mode = mode;
    
    if (mode == PhotoCellModeDelete)
    {
        showToolbar = YES;
    }
    else
    {
        showToolbar = NO;
    }
    
//    [self showToolbar:showToolbar];
}
@end
