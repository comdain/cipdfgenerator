//
//  ImageCollectionViewController.h
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 30/5/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILPDFKit.h"
#import "UIImage+util.h"

@interface ImageCollectionViewController : UIViewController
    
- (instancetype)initWithPhotos:(NSMutableArray*)images ;
@property (nonatomic, strong) UIImage *editedPhoto;

@property (nonatomic, strong) NSMutableArray<UIImage *> *photos;
@property (nonatomic, strong) NSMutableArray<UIImage *> *signaturePhotos;


@property (weak, nonatomic) IBOutlet UICollectionView *imageCollectionview;
@property(nonatomic, assign) NSInteger indexPathsForSelectedItems;
@property (nonatomic, strong) void (^imagedProcessed)(NSArray<UIImage *> *images);


- (instancetype)initWithPhotos:(NSMutableArray*)photos andProcessBlock:(void (^)(NSArray<UIImage *> *images))imageProcessed;

@end
