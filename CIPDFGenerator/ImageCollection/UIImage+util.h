//
//  UIImage+util.h
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 5/6/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (util)
- (BOOL)isLandscape;
@end
