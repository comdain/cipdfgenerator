//
//  ImageCollectionViewController.m
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 30/5/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import "ImageCollectionViewController.h"
#import "ImageCollectionviewCell.h"
#import "PhotoUpdateViewController.h"
#import "ILPDFSignatureController.h"
#import "UIImage+fixOrientation.h"


@interface ImageCollectionViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;

- (IBAction)CameraImage:(id)sender;
- (IBAction)libraryImage:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *noImagesView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cameraButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *LibraryImage;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *LibraryImageAction;

@property (nonatomic, strong) UIImageView *imageview;


@end

@implementation ImageCollectionViewController

- (instancetype)initWithPhotos: (NSMutableArray *)images
    {
        self = [super init];
        if(self)
        {
            self.photos = images;
        }
        return self;
    }


- (instancetype)initWithPhotos:(NSMutableArray*)photos andProcessBlock:(void (^)(NSArray<UIImage *> *images))imageProcessed;
{
    self = [super init];
    
    if (self)
    {
        self.photos = photos;
        self.imagedProcessed = imageProcessed;
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.imageCollectionview.delegate = self;
    self.imageCollectionview.dataSource = self;
    
    [self.imageCollectionview registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell" ];
    [self.imageCollectionview registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"LandscapeCell" ];
    [self.imageCollectionview setBackgroundColor:[UIColor grayColor]];
    
    [self.view addSubview:self.imageCollectionview];
    
    CGFloat width = self.view.frame.size.width;

    UINavigationBar* navbar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0,width-50, 50)];
    
    UINavigationItem* navItem = [[UINavigationItem alloc] initWithTitle:@"Edit Photo"];
    
    UIBarButtonItem* doneBtn = [[UIBarButtonItem alloc]
                                initWithTitle:@"Done"
                                style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonPressed)];
    navItem.rightBarButtonItem = doneBtn;
    
    [navbar setItems:@[navItem]];
    [self.view addSubview:navbar];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed)];
    
    UILongPressGestureRecognizer *lpgr
    = [[UILongPressGestureRecognizer alloc]
       initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.delegate = self;
    lpgr.delaysTouchesBegan = YES;
    [self.imageCollectionview addGestureRecognizer:lpgr];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if(self.editedPhoto !=nil)
    {
        [self.photos replaceObjectAtIndex:_indexPathsForSelectedItems withObject:_editedPhoto];
        [self.imageCollectionview reloadData];
    }
    
    if(self.photos.count>0)
    {
        self.imageCollectionview.hidden = false;
    }
    else
    {
        self.imageCollectionview.hidden = true;
    }
}


#pragma mark - CollectionView delegate and datasource Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
    {
        return [self.photos count];
    }
    
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
    {
        static NSString *identifier = @"Cell";
        ImageCollectionviewCell *cell = [self.imageCollectionview dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];

        ImageCollectionviewCell *Lcell = [self.imageCollectionview dequeueReusableCellWithReuseIdentifier:@"LandscapeCell" forIndexPath:indexPath];
        
        if (self.photos[indexPath.row].isLandscape)
        {
             cell = Lcell;
        }
       
        cell.backgroundColor = [UIColor whiteColor];
        self.imageview = [[UIImageView alloc]initWithFrame:[[cell contentView]frame]];
        
        cell.layer.borderWidth=2.5f;
        cell.layer.borderColor=[UIColor blackColor].CGColor;
      
        self.imageview.image = self.photos[indexPath.row];
        
        [cell addSubview:self.imageview];
        
        return cell;
    }
    

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UIImage *image = self.photos[indexPath.row];
    
    if (image.size.width > image.size.height)
    {
        return CGSizeMake(160.0f, 120.0f);
    }
    else
    {
        return CGSizeMake(120.0f, 160.0f);
    }
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoUpdateViewController *detailVC = [[PhotoUpdateViewController alloc] initWithPhoto:self.photos[indexPath.row] andProcessBlock:^(ILPDFDocument *annotatedPDF, UIImage *image, NSString *assetNumber, NSString *caption) {
        
        self.photos[indexPath.row] = image;
        [self.imageCollectionview reloadData];
    }];
   
    detailVC.modalPresentationStyle = UIModalPresentationPageSheet;
    detailVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:detailVC animated:YES completion:nil];
    self.indexPathsForSelectedItems = indexPath;

}


- (ImageCollectionviewCell *)cellAtIndexPath:(NSIndexPath *)indexPath
{
    return (ImageCollectionviewCell *)[self.imageCollectionview cellForItemAtIndexPath:indexPath];
}


#pragma mark - Navigation buttons

- (void)doneButtonPressed
{
    if (self.imagedProcessed != nil)
    {
        self.imagedProcessed(self.photos);
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
    CGPoint p = [gestureRecognizer locationInView:self.imageCollectionview];
    
    NSIndexPath *indexPath = [self.imageCollectionview indexPathForItemAtPoint:p];
    if (indexPath == nil){
        NSLog(@"couldn't find index path");
    } else {
        // get the cell at indexPath (the one you long pressed)

        self.indexPathsForSelectedItems = indexPath.row;
        
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Delete Image" message:@"Do you want to delete the Image?" preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
//         [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
//        [self presentViewController:alert animated:YES completion:nil];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Image"
                                                        message:@"Do you want to delete the Image?"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Ok",nil];

        [alert show];
  
    }
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        
    }
    else if (buttonIndex == 1)
    {
        [self.photos removeObjectAtIndex:_indexPathsForSelectedItems];
        
        [self.imageCollectionview reloadData];
    }
}


- (IBAction)CameraImage:(id)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
//    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}


- (IBAction)libraryImage:(id)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
//    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:nil];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = [[info objectForKey:@"UIImagePickerControllerOriginalImage"]fixOrientation];
//    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
//    chosenImage = [self imageWithImage:chosenImage scaledToSize:CGSizeMake(2448, 3264)];
    
    [self.photos addObject:chosenImage];
    [self.imageCollectionview reloadData];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
