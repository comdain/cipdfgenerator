//
//  CIPDFAnnotationViewController.h
//  CIPDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 18/7/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILPDFKit.h"

@interface CIPDFAnnotationViewController : UIViewController
- (instancetype)initWithPDFDocument:(ILPDFDocument *)object;

@end
