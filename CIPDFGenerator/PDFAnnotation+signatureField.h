//
//  PDFAnnotation+signatureField.h
//  CIPDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 29/11/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <PDFKit/PDFKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PDFAnnotation (signatureField)

@end

NS_ASSUME_NONNULL_END
