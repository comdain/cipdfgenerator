//
//  UIView+CIPDFView.m
//  CIPDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 6/7/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import "ILPDFKit.h"
#import "UIView+CIPDFView.h"

@implementation UIView (CIPDFView)

-(ILPDFFormTextField *)activePDFTextField
{
    if ([self isKindOfClass:[ILPDFFormTextField class]])
    {
        ILPDFFormTextField *t = (ILPDFFormTextField *)self;
        if(t.textFieldOrTextView.isFirstResponder)
        {
            return t;
        }
    }
    for (UIView *subview in self.subviews)
    {
        ILPDFFormTextField *activeField = [subview activePDFTextField];
        if (activeField != nil)
        {
            return activeField;
        }
    }
    
    return nil;
}


-(void)pinToSuperview: (UIEdgeInsets )insets
{
    UIView *sv = [self superview];
    if (sv)
    {
        [self setTranslatesAutoresizingMaskIntoConstraints:false];
    
        UILayoutGuide *guide = [[UILayoutGuide alloc] init];

        [sv addLayoutGuide:guide];
        
        [guide.topAnchor constraintEqualToAnchor:sv.topAnchor].active = YES;
        [guide.bottomAnchor constraintEqualToAnchor:sv.bottomAnchor].active = YES;
        [guide.leftAnchor constraintEqualToAnchor:sv.leftAnchor].active = YES;
        [guide.rightAnchor constraintEqualToAnchor:sv.rightAnchor].active = YES;
        
        [self pinToSuperview:insets guide:guide];
    }
}


-(void) pinToSuperview : (UIEdgeInsets)insets guide:(UILayoutGuide *)guide
{
    if (self.superview != nil )
    {
        [self setTranslatesAutoresizingMaskIntoConstraints:false];
        
        [self.topAnchor constraintEqualToAnchor:guide.topAnchor
                                        constant:insets.top].active = YES;
        [self.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor
                                           constant:-insets.bottom].active = YES;
        [self.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor
                                            constant:-insets.left].active = YES;
        [self.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor
                                             constant:-insets.right].active = YES;
                
    }
}

@end
