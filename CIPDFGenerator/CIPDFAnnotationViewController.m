//
//  CIPDFAnnotationViewController.m
//  CIPDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 18/7/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import "CIPDFAnnotationViewController.h"

@interface CIPDFAnnotationViewController ()
@property (strong, nonatomic) IBOutlet UIView *drawToolBar;
@end

@implementation CIPDFAnnotationViewController
{
    ILPDFDocument *document;
    
    UIScrollView *theScrollView;
    
    LazyPDFMainToolbar *mainToolbar;

    LazyPDFDrawToolbar *drawToolbar;
    
    UIButton *flattenPDFButton;
    
    LazyPDFMainPagebar *mainPagebar;
    
    UIUserInterfaceIdiom userInterfaceIdiom;
    
    NSMutableDictionary *contentViews;
    
    NSInteger currentPage, minimumPage, maximumPage;
    
    UIDocumentInteractionController *documentInteraction;
    
    UIPrintInteractionController *printInteraction;
    
    CGFloat scrollViewOutset;
    
    CGSize lastAppearSize;
    
    NSDate *lastHideTime;
    
    BOOL ignoreDidScroll;
}

#pragma mark - Constants

#define STATUS_HEIGHT 20.0f

#define TOOLBAR_HEIGHT 44.0f
#define PAGEBAR_HEIGHT 48.0f
#define DRAWBAR_HEIGHT 400.0f
#define DRAWBAR_WIDTH 44.0f

#define SCROLLVIEW_OUTSET_SMALL 4.0f
#define SCROLLVIEW_OUTSET_LARGE 8.0f

#define TAP_AREA_SIZE 48.0f

#pragma mark - UIViewController methods

- (instancetype)initWithPDFDocument:(ILPDFDocument *)object
{
    if ((self = [super initWithNibName:nil bundle:nil])) // Initialize superclass
    {
        if ((object != nil) && ([object isKindOfClass:[ILPDFDocument class]])) // Valid object
        {
            userInterfaceIdiom = [UIDevice currentDevice].userInterfaceIdiom; // User interface idiom
            
            NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter]; // Default notification center
            
            [notificationCenter addObserver:self selector:@selector(applicationWillResign:) name:UIApplicationWillTerminateNotification object:nil];
            
            [notificationCenter addObserver:self selector:@selector(applicationWillResign:) name:UIApplicationWillResignActiveNotification object:nil];
            
            scrollViewOutset = ((userInterfaceIdiom == UIUserInterfaceIdiomPad) ? SCROLLVIEW_OUTSET_LARGE : SCROLLVIEW_OUTSET_SMALL);
            document = object;
//            [object updateDocumentProperties];  // Retain the supplied LazyPDFDocument object for our use
//
//            [LazyPDFThumbCache touchThumbCacheWithGUID:object.guid]; // Touch the document thumb cache directory
        }
        else // Invalid LazyPDFDocument object
        {
            self = nil;
        }
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    assert(document != nil); // Must have a valid PDFDocument
    
    self.view.backgroundColor = [UIColor grayColor]; // Neutral gray
    
    UIView *fakeStatusBar = nil; CGRect viewRect = self.view.bounds; // View bounds
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) // iOS 7+
    {
        if ([self prefersStatusBarHidden] == NO) // Visible status bar
        {
            CGRect statusBarRect = viewRect; statusBarRect.size.height = STATUS_HEIGHT;
            fakeStatusBar = [[UIView alloc] initWithFrame:statusBarRect]; // UIView
            fakeStatusBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            fakeStatusBar.backgroundColor = [UIColor blackColor];
            fakeStatusBar.contentMode = UIViewContentModeRedraw;
            fakeStatusBar.userInteractionEnabled = NO;
            
            viewRect.origin.y += STATUS_HEIGHT; viewRect.size.height -= STATUS_HEIGHT;
        }
    }
    
    CGRect scrollViewRect = CGRectInset(viewRect, -scrollViewOutset, 0.0f);
    theScrollView = [[UIScrollView alloc] initWithFrame:scrollViewRect]; // All
    theScrollView.autoresizesSubviews = NO; theScrollView.contentMode = UIViewContentModeRedraw;
    theScrollView.showsHorizontalScrollIndicator = NO; theScrollView.showsVerticalScrollIndicator = NO;
    theScrollView.scrollsToTop = NO; theScrollView.delaysContentTouches = NO; theScrollView.pagingEnabled = YES;
    theScrollView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    theScrollView.backgroundColor = [UIColor clearColor]; theScrollView.delegate = self;
    [self.view addSubview:theScrollView];
    
    CGRect toolbarRect = viewRect; toolbarRect.size.height = TOOLBAR_HEIGHT;
//    mainToolbar = [[LazyPDFMainToolbar alloc] initWithFrame:toolbarRect document:document]; // LazyPDFMainToolbar
//    mainToolbar.delegate = self; // LazyPDFMainToolbarDelegate
//    [self.view addSubview:mainToolbar];
//    
//    CGRect drawbarRect = CGRectMake(10, viewRect.origin.y+TOOLBAR_HEIGHT+10, DRAWBAR_WIDTH, DRAWBAR_HEIGHT);
//    drawToolbar = [[LazyPDFDrawToolbar alloc] initWithFrame:drawbarRect document:document]; // LazyPDFMainToolbar
//    drawToolbar.delegate = self; // LazyPDFDrawToolbarDelegate
//    [self.view addSubview:drawToolbar];
    
//    CGRect flattenRect = CGRectMake(self.view.bounds.size.width-120, viewRect.origin.y+TOOLBAR_HEIGHT+10, 110, 40);
//    flattenPDFButton = [[UIButton alloc] initWithFrame:flattenRect];
//    [flattenPDFButton setTitle:@"Flatten PDF" forState:UIControlStateNormal];
//    [flattenPDFButton setTintColor:[UIColor blueColor]];
//    [flattenPDFButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
//    [flattenPDFButton addTarget:self action:@selector(flattenPDF) forControlEvents:UIControlEventTouchUpInside];
//    //[[flattenPDFButton layer] setBorderWidth:2.0];
//    //[[flattenPDFButton layer] setBorderColor:[UIColor blueColor].CGColor];
//    [self.view addSubview:flattenPDFButton];
//    
//    CGRect pagebarRect = self.view.bounds; pagebarRect.size.height = PAGEBAR_HEIGHT;
//    pagebarRect.origin.y = (self.view.bounds.size.height - pagebarRect.size.height);
//    mainPagebar = [[LazyPDFMainPagebar alloc] initWithFrame:pagebarRect document:document]; // LazyPDFMainPagebar
//    mainPagebar.delegate = self; // LazyPDFMainPagebarDelegate
//    [self.view addSubview:mainPagebar];
//    
//    if (fakeStatusBar != nil) [self.view addSubview:fakeStatusBar]; // Add status bar background view
//    
//    UITapGestureRecognizer *singleTapOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
//    singleTapOne.numberOfTouchesRequired = 1; singleTapOne.numberOfTapsRequired = 1; singleTapOne.delegate = self;
//    [self.view addGestureRecognizer:singleTapOne];
//    
//    UITapGestureRecognizer *doubleTapOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
//    doubleTapOne.numberOfTouchesRequired = 1; doubleTapOne.numberOfTapsRequired = 2; doubleTapOne.delegate = self;
//    [self.view addGestureRecognizer:doubleTapOne];
//    
//    UITapGestureRecognizer *doubleTapTwo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
//    doubleTapTwo.numberOfTouchesRequired = 2; doubleTapTwo.numberOfTapsRequired = 2; doubleTapTwo.delegate = self;
//    [self.view addGestureRecognizer:doubleTapTwo];
//    
//    [singleTapOne requireGestureRecognizerToFail:doubleTapOne]; // Single tap requires double tap to fail
//    
//    contentViews = [NSMutableDictionary new]; lastHideTime = [NSDate date];
//    
//    minimumPage = 1; maximumPage = [document.pageCount integerValue];
//    
//    [self.view bringSubviewToFront:self.drawToolBar];
//    self.drawToolBar.userInteractionEnabled = YES;
//    [self updateButtonStatus];
//    self.lineWidth = [NSNumber numberWithFloat:2.0];
//    self.lineAlpha = [NSNumber numberWithFloat:1.0];
//    self.lineColor = [UIColor blueColor];
    
    [self.view bringSubviewToFront:flattenPDFButton];
    [flattenPDFButton setUserInteractionEnabled:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
