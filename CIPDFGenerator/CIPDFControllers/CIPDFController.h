//
//  CIPDFController.h
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 15/6/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ILPDFKit.h"
#import "CIPDFViewController.h"
#import "CIAnnotationView.h"
#import "PDFAnnotations.h"


@interface PDFQueueElement : NSObject 

@property (strong,nonatomic) NSString *sourcePdfName;
@property (strong,nonatomic) NSString *elementName;
@property (strong,nonatomic) NSString *targetPdfName;
@property (strong,nonatomic) ILPDFDocument *targetDocument;
@property (strong,nonatomic) ILPDFDocument *targetPDFDocument;
@property (strong,nonatomic) NSData *originalDocumentData;
@property (nonatomic, strong) NSMutableArray<PDFAnnotations *> *annotations;
@property (nonatomic, strong) NSMutableArray *signatureAnnottaions;
@property (nonatomic, strong) NSMutableArray *replaceWithTheseSignatureAnnotation;

-(instancetype)initWithSourcePdfName:(NSString *)sourcePdfName elementName:(NSString *)elementName targetPdfName:(NSString *)targetPdfName andTargetDocument:(ILPDFDocument *)targetDocument;

-(instancetype)initWithPdfName:(NSString *)pdfName andOriginalDocumentData:(NSData *)originalDocumentData;

-(instancetype)initWithPdfQueueElement:(PDFQueueElement *)pdfQueueElement;


@end

@interface CIPDFController : NSObject
@property (strong,nonatomic) ILPDFViewController *pdfViewController;
@property (strong,nonatomic) NSMutableArray<PDFQueueElement *> *pdfRenderingQueue;
@property (nonatomic) NSUInteger currentIndex;
@property (strong,nonatomic) NSArray<PDFQueueElement *> *allPdfs;
@property (strong,nonatomic) NSString *firstSourcePDFName;
@property (strong,nonatomic) NSString *currentSourcePDFName;
@property (nonatomic, assign) BOOL nextPdf;
@property (strong,nonatomic) UIImage *combinedImage;
@property (strong,nonatomic) UIImage *combinedSignatureImage;
@property (strong,nonatomic) NSMutableArray *combinedImagesArray;
@property (strong,nonatomic) NSMutableArray *combinedSignatureArray;
@property (nonatomic, strong) NSMutableArray *annotatedDocuments;
@property (nonatomic, strong) UIImageView *annotationImageView;
@property (nonatomic, strong) NSMutableArray *annotationImageViews;
@property (nonatomic, weak) ILPDFView<UIScrollViewDelegate> *pdfView;

@property (strong,nonatomic) NSMutableArray<PDFQueueElement *> *nextPdfs;

-(instancetype)initWithPdfs:(NSArray<PDFQueueElement *>*)allPdfs firstSourcePDFName:(NSString *)firstSourcePDFName andPdfViewController:(ILPDFViewController *)pdfViewController;

-(ILPDFDocument *)combineImages:(NSArray *)images andumberOfImagesPerPage:(NSUInteger )numberOfImagesPerPage andDocumentToAppend: (ILPDFDocument *)document;
-(ILPDFDocument *)combineImages:(NSArray *)images andDocumentToAppend: (ILPDFDocument *)document;

-(void)go;
-(void)next;
-(void)previous;

-(ILPDFDocument *)mergePDFToMakeFInalPDF;
-(UIImage*) drawText:(NSString*) text inImage:(UIImage*) image atPoint:(CGPoint) point;
- (void)populateCaptions;
-(NSArray *)annottaionArrayOnCurrentQueueElement;
-(void)doneButtonPressed;

@end
