//
//  CIPDFViewController.m
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 21/6/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.


#import "CIPDFViewController.h"
#import "signatureViewController.h"
#import "ILPDFSignatureController.h"
#import "ImageCollectionViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "CIAnnotationView.h"

#import "ILPDFFormContainer.h"

@interface CIPDFViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) NSArray<PDFQueueElement *> *allPDFs;

@property (nonatomic, strong) CIPDFController *pdfController;


@property (nonatomic, strong) ILPDFDocument *pdfDocument;
@property (nonatomic, strong) ILPDFDocument *mergedDocument;
@property (nonatomic, strong) ILPDFView *pdfView;
@property (nonatomic, strong) CIAnnotationView *annotationV;
@property (nonatomic, strong) UIBarButtonItem *edit;
@property (nonatomic, strong) UIBarButtonItem *next;
@property (nonatomic, strong) UIBarButtonItem *annot;
@property (nonatomic, strong) UIBarButtonItem *done;
@property (nonatomic, strong) UIBarButtonItem *camera;
@property (nonatomic, strong) UIBarButtonItem *signature;
@property (nonatomic, strong) UIBarButtonItem *undo;
@property (nonatomic, strong) UIBarButtonItem *erase;
@property (nonatomic, strong) UIBarButtonItem *color;
@property (nonatomic, strong) UIBarButtonItem *highlighter;

@property (nonatomic, strong) PDFQueueElement *currentPdfQueueElement;
@property (nonatomic, strong) UILongPressGestureRecognizer *longPressGestureRecognizer;

@end

@implementation CIPDFViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.imagesTaken = [NSMutableArray array];
    self.signaturesCollected = [NSMutableArray array];
    
    self.numberOfImagesPerPage = 2;
    
    [self prepareNavigation];

    [self createPDFController];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSArray *)generateAllPDFs
{
//  Subclasses should override this method
    return nil;
}


-(void)createPDFController
{
    self.allPDFs = [self generateAllPDFs];

    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    self.pdfVC = [storyboard instantiateViewControllerWithIdentifier:@"pdfVC"];
    [self setNavigationbarButtons];
    
    [self.navigationController pushViewController:self.pdfVC animated:YES];
    
    if (self.allPDFs.count > 0)
    {
        NSString *firstPDFName = self.allPDFs[0].sourcePdfName;
        self.pdfController = [[CIPDFController alloc] initWithPdfs:self.allPDFs firstSourcePDFName:firstPDFName andPdfViewController:self.pdfVC];
    }
    
    [self.pdfController go];
}


#pragma mark - Navigation bar
-(void)setNavigationbarButtons
{
    NSArray *navigationButtonArray = [NSArray arrayWithObjects:self.next, self.camera, self.signature, self.edit, nil];
    self.pdfVC.navigationItem.rightBarButtonItems = navigationButtonArray;
    self.pdfVC.navigationItem.hidesBackButton = NO;
    [self showPreviousButton];
    [self.pdfVC.navigationController.navigationBar removeGestureRecognizer:self.longPressGestureRecognizer];
}


-(void)setNavigationBarButtonsForEditMode
{
    NSArray *navigationButtonArray = [NSArray arrayWithObjects:self.done, self.annot, self.highlighter, self.color, self.undo, self.erase, nil];
    self.pdfVC.navigationItem.rightBarButtonItems = navigationButtonArray;
    self.pdfVC.navigationItem.hidesBackButton = YES;
    self.pdfVC.navigationItem.leftBarButtonItem = nil;
    
    CGRect rect = CGRectMake(0, 0, 20, 20);
    self.longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    UIView *myTransparentGestureView = [[UIView alloc] initWithFrame:rect];
    myTransparentGestureView.backgroundColor = [UIColor clearColor];
    [myTransparentGestureView addGestureRecognizer:self.longPressGestureRecognizer];
    
    [self.pdfVC.navigationController.navigationBar addGestureRecognizer:self.longPressGestureRecognizer];
}


-(void)nextButtonPressed:(UIButton *)sender
{
    self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    self.pdfDocument = [[ILPDFDocument alloc]initWithData:self.pdfVC.pdfView.document.dataRepresentation];
    
    self.pdfDocument.replaceWithTheseSignatureAnnotations = self.pdfVC.document.replaceWithTheseSignatureAnnotations;
    
    self.pdfController.pdfRenderingQueue[self.pdfController.currentIndex].targetPDFDocument = self.pdfDocument;
    self.pdfController.pdfRenderingQueue[self.pdfController.currentIndex].targetDocument = self.pdfDocument;

    NSArray *requiredFields = [NSArray array];

    requiredFields = self.pdfDocument.getRequiredFields;

    if (requiredFields.count == 0)
    {
        self.pdfController.pdfViewController.PDFViewDocument = self.pdfDocument;

        [self.pdfController next];
        
        [self showPreviousButton];
       
        bool next = self.pdfController.nextPdf;

        if (next == false)
        {
            UIWindow* mainWindow = [[[[UIApplication sharedApplication] keyWindow] subviews] lastObject];
            [mainWindow addSubview: self.activityIndicatorView];
            [self.activityIndicatorView startAnimating];

            self.mergedDocument = [self.pdfController mergePDFToMakeFInalPDF];

            if (self.finalImages.count != 0)
            {
                self.mergedDocument = [self.pdfController combineImages:self.finalImages andumberOfImagesPerPage:self.numberOfImagesPerPage andDocumentToAppend:self.mergedDocument];
            }
            self.signaturesWithNameAndDate = [[NSMutableArray alloc]init];

            for(int i=0 ; i < self.finalSignatures.count ; i++)
            {
                NSString *stringToAdd =  [NSString stringWithFormat:@"%@\r\r%@", [[self.finalSignatures objectAtIndex:i] objectForKey:@"Name"],[[self.finalSignatures objectAtIndex:i] objectForKey:@"Date"]];
                UIImage *img;
                if (i==0)
                {
                    NSString *stringToAdd =  [NSString stringWithFormat:@"%@\r\r%@\r\r%@", @"                                              Signed By:",[[self.finalSignatures objectAtIndex:i] objectForKey:@"Name"],[[self.finalSignatures objectAtIndex:i] objectForKey:@"Date"]];
                    CIPDFController *cipdfController = [[CIPDFController alloc]init];
                    img = [cipdfController drawText:stringToAdd
                                 inImage:[[self.finalSignatures objectAtIndex:i] objectForKey:@"SigntureImage"]
                                 atPoint:CGPointMake(100,0)];
                }
                else
                {
                    img = [self.pdfController drawText:stringToAdd
                                 inImage:[[self.finalSignatures objectAtIndex:i] objectForKey:@"SigntureImage"]
                                 atPoint:CGPointMake(100,100)];
                }
                [self.signaturesWithNameAndDate addObject:img];
            }

            if (self.signaturesWithNameAndDate.count != 0)
            {
                self.mergedDocument = [self.pdfController combineImages:self.signaturesWithNameAndDate andDocumentToAppend:self.mergedDocument];
            }

            self.pdfVC.document = self.mergedDocument;
            
//             NSData *data = self.pdfDocument.savedStaticPDFData;
//
//            PDFDocument *doc = [[PDFDocument alloc]initWithData:data];
            
            PDFDocument *dc = [[PDFDocument alloc]initWithData:self.mergedDocument.documentData];
        
            self.pdfVC.pdfView.document = dc;
        }
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Required Fields" message:[NSString stringWithFormat:@"Please fill %@", requiredFields] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

//- (NSData *)savedStaticPDFData{
////    ILPDFViewController *pdfViewController = [[ILPDFViewController alloc]init];
////    ILPDFDocument *ilpdfDoc = [[ILPDFDocument alloc]initWithDocumentRef:pdfViewController.pdfView.document.documentRef];
//
//    NSUInteger numberOfPages = self.pdfVC.pdfView.visiblePages.count;
//    NSMutableData *pageData = [NSMutableData data];
//    UIGraphicsBeginPDFContextToData(pageData, CGRectZero , nil);
//    CGContextRef ctx = UIGraphicsGetCurrentContext();
//    for (NSUInteger page = 1; page <= numberOfPages; page++) {
//        renderPage(page, ctx, _document , self.forms, nil);
//    }
//    UIGraphicsEndPDFContext();
//    return pageData;
//}


//static void renderPage(NSUInteger page, CGContextRef ctx, CGPDFDocumentRef doc, ILPDFFormContainer *forms, NSArray *annotations) {
//    CGRect mediaRect = CGPDFPageGetBoxRect(CGPDFDocumentGetPage(doc,page), kCGPDFMediaBox);
//    CGRect cropRect = CGPDFPageGetBoxRect(CGPDFDocumentGetPage(doc,page), kCGPDFCropBox);
//    CGRect artRect = CGPDFPageGetBoxRect(CGPDFDocumentGetPage(doc,page), kCGPDFArtBox);
//    CGRect bleedRect = CGPDFPageGetBoxRect(CGPDFDocumentGetPage(doc,page), kCGPDFBleedBox);
//    UIGraphicsBeginPDFPageWithInfo(mediaRect, @{(NSString*)kCGPDFContextCropBox:[NSValue valueWithCGRect:cropRect],(NSString*)kCGPDFContextArtBox:[NSValue valueWithCGRect:artRect],(NSString*)kCGPDFContextBleedBox:[NSValue valueWithCGRect:bleedRect]});
//    CGContextSaveGState(ctx);
//    CGContextScaleCTM(ctx,1,-1);
//    CGContextTranslateCTM(ctx, 0, -mediaRect.size.height);
//    CGContextDrawPDFPage(ctx, CGPDFDocumentGetPage(doc,page));
//    CGContextRestoreGState(ctx);
//    for (ILPDFForm *form in forms) {
//        if (form.page == page) {
//            CGContextSaveGState(ctx);
//            CGRect frame = form.frame;
//            CGRect correctedFrame = CGRectMake(frame.origin.x-mediaRect.origin.x, mediaRect.size.height-frame.origin.y-frame.size.height-mediaRect.origin.y, frame.size.width, frame.size.height);
//            CGContextTranslateCTM(ctx, correctedFrame.origin.x, correctedFrame.origin.y);
//
//            [form vectorRenderInPDFContext:ctx forRect:correctedFrame];
//            CGContextRestoreGState(ctx);
//        }
//    }
//
//    NSMutableArray *annotationImageViews = [NSMutableArray array];
//    NSMutableArray *finalAnnotationImageViews = [NSMutableArray array];
//    CGRect correctedFrame;
//    CGRect frame;
//
//    for (PDFAnnotations *annotation in annotations) {
//        if (annotation.pageNumber == page) {
//            CGContextSaveGState(ctx);
//            frame = annotation.zoomView.frame;
//            correctedFrame = CGRectMake(frame.origin.x-mediaRect.origin.x, mediaRect.size.height-frame.origin.y-frame.size.height-mediaRect.origin.y, frame.size.width, frame.size.height);
//
//            CGContextTranslateCTM(ctx,0, 0);
//
//            for (UIImageView *annotImageView in annotationImageViews)
//            {
//                if (CGRectEqualToRect(annotation.zoomView.frame, annotImageView.frame))
//                {
//                    [finalAnnotationImageViews removeObject:annotImageView];
//                }
//            }
//
//            UIImageView *imageView = [[UIImageView alloc] initWithFrame:annotation.zoomView.frame];
//            imageView.image = [annotation.incrementImages lastObject];
//
//            [annotationImageViews addObject:imageView];
//            [finalAnnotationImageViews addObject:imageView];
//
//            CGContextRestoreGState(ctx);
//        }
//    }
//
//    UIImageView *finalImageView = [finalAnnotationImageViews lastObject];
//    [finalImageView.image drawInRect:CGRectMake (0, 0, correctedFrame.size.width-120, correctedFrame.size.height-170)];
//
//}

-(void)showPreviousButton
{
    UIBarButtonItem *previous = [[UIBarButtonItem alloc]
                             initWithTitle:@"Previous"
                             style:UIBarButtonItemStylePlain
                             target:self
                             action:@selector(previousButtonPressed:)];
    
    self.pdfVC.navigationItem.leftBarButtonItem = previous;
}


-(void)previousButtonPressed:(UIButton *)sender
{
    [self.pdfController previous];
}


-(void)collectImages: (UIButton *)sender
{
    __weak typeof(self) weakSelf = self;
    ImageCollectionViewController *imageCollectionVC;
    imageCollectionVC = [[ImageCollectionViewController alloc]initWithPhotos:weakSelf.imagesTaken
                        andProcessBlock:^(NSArray<UIImage *> *images) {
    weakSelf.finalImages = images;

}];
    
    imageCollectionVC.modalPresentationStyle = UIModalPresentationFormSheet;
    [self.navigationController presentViewController:imageCollectionVC animated:YES completion:nil];
    
}


-(void)collectSignatures: (UIButton *)sender
{
    __weak typeof(self) weakSelf = self;

    signatureViewController *signatureVC = [[signatureViewController alloc]initWithSignature:self.signaturesCollected andProcessBlock:^(NSArray *signatures)
    {
        weakSelf.finalSignatures = signatures;

    }];

    signatureVC.modalPresentationStyle = UIModalPresentationFormSheet;
    [self.pdfVC.parentViewController presentViewController:signatureVC animated:YES completion:nil];

}


#pragma mark - Navigation Buttons

- (void)editPDF: (UIButton *)sender
{
    if (self.pdfController.annotationImageViews != nil)
    {
        for (CIAnnotationView *annotationview in self.pdfController.annotationImageViews)
        {
            [annotationview removeFromSuperview];
        }
    }
    
    self.pdfDocument = [self.pdfController.pdfRenderingQueue[self.pdfController.currentIndex] targetDocument];

    [self setNavigationBarButtonsForEditMode];
    
    BOOL viewIsPresent = NO;

    self.currentPdfQueueElement = self.pdfController.pdfRenderingQueue[self.pdfController.currentIndex];
    
    if (self.currentPdfQueueElement.annotations != nil)
    {
         viewIsPresent = YES;
    }
    
    if (self.annotationV == nil || !viewIsPresent )
    {
        self.annotationV = [[CIAnnotationView alloc] initWithPDFView:self.pdfVC.pdfView andpdfDocument:self.pdfDocument andAnnottaions:nil] ;
    }
    
    else
    {
        NSLog(@"previous annotions are: %@", self.currentPdfQueueElement.annotations);
        
        self.annotationV = [[CIAnnotationView alloc] initWithPDFView:self.pdfVC.pdfView andpdfDocument:self.pdfDocument andAnnottaions:self.currentPdfQueueElement.annotations] ;
        
    }
        self.annotationV.annotatedDocumentDelegate = self.pdfController;
    
        [self.pdfVC.view addSubview:self.annotationV];
}


- (void)annot: (UIButton *)sender
{
    [self.annotationV setStrokeColor:[UIColor blackColor]];
    if (self.strokeWidth == 0)
    {
        self.strokeWidth = 4;
    }
    [self.annotationV setStrokeWidth:self.strokeWidth];
    [self.annotationV doAnnotation];
}


- (void)highLight: (UIButton *)sender
{
    [self.annotationV setStrokeColor:[UIColor colorWithRed:4.0 green:16.0 blue:0 alpha:0.3]];
    [self.annotationV setStrokeWidth:20];
    [self.annotationV doAnnotation];
}


-(void)doneButtonPressed: (UIButton *)sender
{
    [self.pdfController doneButtonPressed];
    
    [self.annotationV removeFromSuperview];
    [self setNavigationbarButtons];
}


- (void)undoButtonPressed: (UIButton *)sender
{
    [self.annotationV undoPressed];
}


- (void)longPress:(UILongPressGestureRecognizer *)gesture
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Choose Stroke size" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *small =[UIAlertAction actionWithTitle:@"Small" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//        [self.annotationV setStrokeColor:[UIColor blackColor]];
        [self.annotationV setStrokeWidth:4];
        self.strokeWidth = 4;
        [self.annotationV doAnnotation];
        
    }];
    
    [small setValue:[UIImage imageNamed:@"stroke1"] forKey:@"image"];
    
    [actionSheet addAction:small];

    UIAlertAction *medium =[UIAlertAction actionWithTitle:@"Medium" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self.annotationV setStrokeColor:[UIColor blackColor]];
        [self.annotationV setStrokeWidth:10];
        self.strokeWidth = 10;
        [self.annotationV doAnnotation];
        
    }];
    [medium setValue:[UIImage imageNamed:@"stroke2"] forKey:@"image"];
    
    [actionSheet addAction:medium];
    
    UIAlertAction *large =[UIAlertAction actionWithTitle:@"Large" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self.annotationV setStrokeColor:[UIColor blackColor]];
        [self.annotationV setStrokeWidth:25];
        self.strokeWidth = 25;
        [self.annotationV doAnnotation];
    }];

    [large setValue:[UIImage imageNamed:@"stroke3"] forKey:@"image"];
    
    [actionSheet addAction:large];
    
    UIBarButtonItem *item = [self.pdfVC.navigationItem.rightBarButtonItems objectAtIndex:1] ;
    UIView *view = [item valueForKey:@"view"];
    CGFloat width;
    CGRect rect;
    {
        width=[view frame].size.width;
        rect = view.frame;
    }

    actionSheet.popoverPresentationController.barButtonItem = item;
    actionSheet.popoverPresentationController.sourceView = view;
    actionSheet.popoverPresentationController.sourceRect = rect;
   
    [self presentViewController:actionSheet animated:YES completion:nil];
}


- (void)eraseAnnotation: (UIButton *)sender
{
    [self.annotationV erasePressed];
}

- (void)strokeColor: (UIButton *)sender
{
//    UIView *colorView = [[UIView alloc]initWithFrame:CGRectMake(570, 0, 70, 200)];
//    colorView.backgroundColor = [UIColor grayColor];
//
//    UIButton *blackColor = [[UIButton alloc]init];
//    blackColor = [self cicleButtonWithRect:CGRectMake(20.0, 25.0, 30.0, 30.0) andColor:[UIColor blackColor]];
//    UIButton *greenColor = [self cicleButtonWithRect:CGRectMake(20.0, 145.0, 30.0, 30.0) andColor:[UIColor greenColor]];
//    UIButton *yellowColor = [self cicleButtonWithRect:CGRectMake(20.0, 105.0, 30.0, 30.0) andColor:[UIColor yellowColor]];
//    UIButton *whiteColor = [self cicleButtonWithRect:CGRectMake(20.0, 65.0, 30.0, 30.0) andColor:[UIColor whiteColor]];
//
//    [blackColor addTarget:self
//               action:@selector(pickedColor:)
//     forControlEvents:UIControlEventTouchUpInside];
//
//    [colorView addSubview:blackColor];
//    [colorView addSubview:greenColor];
//    [colorView addSubview:yellowColor];
//    [colorView addSubview:whiteColor];
//    [self.pdfVC.pdfView.pdfView addSubview:colorView];
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Choose Color" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"White" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self.annotationV setStrokeColor:[UIColor whiteColor]];
        
        [self.annotationV doAnnotation];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Black" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self.annotationV setStrokeColor:[UIColor blackColor]];
        [self.annotationV doAnnotation];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Yellow" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self.annotationV setStrokeColor:[UIColor yellowColor]];
        
        [self.annotationV doAnnotation];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Green" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self.annotationV setStrokeColor:[UIColor greenColor]];
        [self.annotationV doAnnotation];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Red" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self.annotationV setStrokeColor:[UIColor redColor]];
        [self.annotationV doAnnotation];
    }]];
    // Present action sheet.
    
    UIBarButtonItem *item = [self.pdfVC.navigationItem.rightBarButtonItems objectAtIndex:3] ;
    UIView *view = [item valueForKey:@"view"];
    CGFloat width;
    CGRect rect;
    {
        width=[view frame].size.width;
        rect = view.frame;
    }
    
    actionSheet.popoverPresentationController.barButtonItem = item;
    actionSheet.popoverPresentationController.sourceView = view;
    actionSheet.popoverPresentationController.sourceRect = rect;
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}


-(UIButton *)cicleButtonWithRect: (CGRect )rect andColor: (UIColor *)color
{
    UIButton *button = [[UIButton alloc]init];
  
    
    button.backgroundColor = color;
    button.frame = rect;
    button.layer.cornerRadius = rect.size.width / 2 ;
    
    return button;
}


#pragma mark - Navigation Buttons

-(void) prepareNavigation
{
    self.done = [[UIBarButtonItem alloc]
                 initWithTitle:@"Done"
                 style:UIBarButtonItemStylePlain
                 target:self
                 action:@selector(doneButtonPressed:)];
    
    self.camera = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(collectImages:)];
    
    self.signature = [[UIBarButtonItem alloc]
                      initWithTitle:@"Sign"
                      style:UIBarButtonItemStylePlain
                      target:self
                      action:@selector(collectSignatures:)];
    
    self.annot = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"annot"] style:UIBarButtonItemStylePlain target:self action:@selector(annot:)];
    self.highlighter = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"highlight"] style:UIBarButtonItemStylePlain target:self action:@selector(highLight:)];
    
    self.next = [[UIBarButtonItem alloc]
                 initWithTitle:@"Next"
                 style:UIBarButtonItemStylePlain
                 target:self
                 action:@selector(nextButtonPressed:)];
    
    self.undo = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"undo"] style:UIBarButtonItemStylePlain target:self action:@selector(undoButtonPressed:)];
    
    self.edit = [[UIBarButtonItem alloc]
                 initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editPDF:)];
    
    self.erase = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"erase"] style:UIBarButtonItemStylePlain target:self action:@selector(eraseAnnotation:)];
    
    self.color = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"color"] style:UIBarButtonItemStylePlain target:self action:@selector(strokeColor:)];
}


@end
