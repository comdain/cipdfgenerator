  //
//  CIPDFController.m
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 15/6/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import "CIPDFController.h"
#import "NSString+Utils.h"
#import "UIImage+Utils.h"
#import "PDFImageConverter.h"

@interface CIPDFController() <AnnotatedDocumentDelegate, WKNavigationDelegate>

@property (nonatomic, strong) PDFQueueElement *currentPDFQueueElement;
// @property (nonatomic, weak) ILPDFView<UIScrollViewDelegate> *pdfView;

@end

@implementation CIPDFController

-(instancetype)initWithPdfs:(NSArray<PDFQueueElement *>*)allPdfs firstSourcePDFName:(NSString *)firstSourcePDFName andPdfViewController:(ILPDFViewController *)pdfViewController
{
    self = [super init];
    
    if (self)
    {
        self.allPdfs = allPdfs;
        self.pdfViewController = pdfViewController;
        self.firstSourcePDFName = firstSourcePDFName;
        self.currentIndex = 0;
        
        self.annotatedDocuments = [NSMutableArray array];
    }
    return self;
}


-(void)go
{
    self.currentIndex = 0;
    self.nextPdfs = [NSMutableArray array];
    if (self.allPdfs.count > 0)
    {
        self.pdfRenderingQueue = [NSMutableArray array];
        
        PDFQueueElement *firstPdf = [[PDFQueueElement alloc] initWithPdfQueueElement:self.allPdfs[0]];
        
        firstPdf.sourcePdfName = self.firstSourcePDFName;
        
        [self.pdfRenderingQueue addObject:firstPdf];
        
        [self renderPDFAtCurrentIndex];
        
    }
}


- (void)next
{
    NSMutableDictionary *pdfList = [[NSMutableDictionary alloc]init];
    NSDictionary *pdfNameList = [[NSDictionary alloc]init];
    NSDictionary *selectedButtons = [[NSDictionary alloc]init];
    
    pdfNameList = self.pdfViewController.document.getPDFNames;
    
    if(self.pdfViewController.document.linkedPDFNames == nil)
    {
        self.pdfViewController.PDFViewDocument.linkedPDFNames = [NSDictionary dictionary];
        self.pdfViewController.PDFViewDocument.linkedPDFNames = pdfNameList;
    }
    
    selectedButtons = self.pdfViewController.PDFViewDocument.getSelectedFields;

    NSMutableSet *keysInA;
     if(self.pdfViewController.document.linkedPDFNames == nil)
     {
         keysInA = [NSMutableSet setWithArray:[self.pdfViewController.PDFViewDocument.linkedPDFNames allKeys]];
          self.pdfViewController.document.linkedPDFNames =  self.pdfViewController.PDFViewDocument.linkedPDFNames = pdfNameList;;
     }
    else
    {
        keysInA = [NSMutableSet setWithArray:[self.pdfViewController.document.linkedPDFNames allKeys]];
    }
    
    NSSet *keysInB = [NSSet setWithArray:[selectedButtons allKeys]];
  
    [keysInA intersectSet:keysInB];
    
    for (id key in keysInA)
    {
        [pdfList setValue:[self.pdfViewController.document.linkedPDFNames objectForKey:key] forKey:key];
    }

    for (NSString *key in pdfList)
    {
            NSString *pdfName = pdfList[key];
        
            NSString *modifiedKey = [[key componentsSeparatedByString:@"_"] objectAtIndex:0];
        
            PDFQueueElement *newElement = [[PDFQueueElement alloc] initWithSourcePdfName:self.currentSourcePDFName elementName:modifiedKey targetPdfName:pdfName andTargetDocument:nil];
        
        if ([self.pdfRenderingQueue containsObject:newElement])
        {
            PDFQueueElement *existingElement = [self findElementMatchingSourcePDFName:newElement.sourcePdfName andElementName:newElement.elementName];
            
            if (![newElement.targetPdfName isEqualToString:existingElement.targetPdfName])
            {
                if (StringIsEmpty(newElement.targetPdfName))
                {
                    NSArray *removalArray = [self addRelatedEntriesForTargetPdf:existingElement toArray:nil];
                    for (PDFQueueElement* element in removalArray)
                    {
                        [self.pdfRenderingQueue removeObject:element];
                    }
                }
                else
                {
                    NSArray *removalArray = [self addRelatedEntriesForTargetPdf:existingElement toArray:nil];
                    for (PDFQueueElement* element in removalArray)
                    {
                        [self.pdfRenderingQueue removeObject:element];
                    }
                    
                    if (!StringIsEmpty(newElement.targetPdfName))
                    {
                        PDFQueueElement *allPDFsElement = [self findElementInAllPDFsMatchingTargetPDFName:newElement.targetPdfName];
                        
                        if (allPDFsElement != nil)
                        {
                            newElement.originalDocumentData = allPDFsElement.originalDocumentData;
                            [self insertOrAddNewElement:newElement];
                        }
                    }
                }
            }
        }
        else
        {
            if (!StringIsEmpty(newElement.targetPdfName))
            {
                PDFQueueElement *allPDFsElement = [self findElementInAllPDFsMatchingTargetPDFName:newElement.targetPdfName];
                
                if (allPDFsElement != nil)
                {
                    newElement.originalDocumentData = allPDFsElement.originalDocumentData;
                    [self insertOrAddNewElement:newElement];
                }
            }
        }
    }
//     self.pdfViewController.document = self.pdfViewController.PDFViewDocument;
    [self displayNextPDF];
    
}


-(NSMutableArray *)addRelatedEntriesForTargetPdf:(PDFQueueElement*)targetElement toArray: (NSMutableArray *)removalArray
{
    if(removalArray == nil)
    {
        removalArray = [NSMutableArray array];
        [removalArray addObject:targetElement];
    }
    
    NSMutableArray *localArray = [NSMutableArray array];
    
    for (PDFQueueElement *queueElement in self.pdfRenderingQueue)
    {
        if (queueElement.sourcePdfName == targetElement.targetPdfName)
        {
            [localArray addObject:queueElement];
            [removalArray addObject:queueElement];
        }
    }
    
    for (PDFQueueElement *queueElement in localArray)
    {
        removalArray = [self addRelatedEntriesForTargetPdf:queueElement toArray:removalArray ];
    }
    
    return  removalArray;
}


- (void)displayNextPDF
{
    if (self.pdfRenderingQueue.count > self.currentIndex + 1)
    {
        self.nextPdf = YES;
        self.currentIndex++;
        
        [self renderPDFAtCurrentIndex];
    }
    else
    {
        [self printAlert];
        self.nextPdf = false;
    }
}


- (void)insertOrAddNewElement:(PDFQueueElement *)newElement
{
    if (self.pdfRenderingQueue.count >= self.currentIndex)
    {
        [self.pdfRenderingQueue insertObject:newElement atIndex:self.currentIndex + 1];
    }
    else
    {
        [self.pdfRenderingQueue addObject:newElement];
    }
}


- (void)previous
{
    if (self.currentIndex != 0)
    {
        ILPDFDocument *pdfDocument = [[ILPDFDocument alloc]initWithData:self.pdfViewController.pdfView.document.dataRepresentation];
        pdfDocument.replaceWithTheseSignatureAnnotations = self.pdfViewController.document.replaceWithTheseSignatureAnnotations;
        self.pdfRenderingQueue[self.currentIndex].targetPDFDocument = pdfDocument ;
        self.pdfRenderingQueue[self.currentIndex].targetDocument = pdfDocument;
        
//         self.pdfRenderingQueue[self.currentIndex].targetDocument.replaceWithTheseSignatureAnnotations = self.pdfViewController.document.replaceWithTheseSignatureAnnotations;

        self.currentIndex--;
        [self renderPDFAtCurrentIndex];
    }
}


- (void)renderPDFAtCurrentIndex
{
    if (self.currentIndex < self.pdfRenderingQueue.count)
    {
        
        self.currentPDFQueueElement = [self.pdfRenderingQueue objectAtIndex:self.currentIndex];
        self.currentSourcePDFName = self.currentPDFQueueElement.targetPdfName;
        ILPDFDocument *document = self.currentPDFQueueElement.targetDocument;
        ILPDFDocument *pdfDocument = self.currentPDFQueueElement.targetPDFDocument;
        
      
        
        if (document != nil)
        {
            self.pdfViewController.document = document;
            
            self.pdfViewController.PDFViewDocument = pdfDocument;
            
        }
    }
}


- (void)renderAnnotations
{
//    self.pdfViewController.pdfView.pdfView.scrollView.zoomScale = 1;
    self.annotationImageViews = [NSMutableArray array];
    NSMutableArray *finalAnnotationImageViews = [NSMutableArray array];
    
    self.currentPDFQueueElement = self.pdfRenderingQueue[self.currentIndex];
    
    for (PDFAnnotations *annotation in self.currentPDFQueueElement.annotations)
    {
        for (UIImageView *annotImageView in self.annotationImageViews)
        {
            if (CGRectEqualToRect(annotation.zoomView.frame, annotImageView.frame))
            {
                [finalAnnotationImageViews removeObject:annotImageView];
            }
        }
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:annotation.zoomView.frame];
        imageView.image = [annotation.incrementImages lastObject];
        
        [self.annotationImageViews addObject:imageView];
        [finalAnnotationImageViews addObject:imageView];
        
    }
    for (UIImageView *annotImageView in finalAnnotationImageViews)
    {
//        [self.pdfViewController.pdfView.pdfView.scrollView addSubview:annotImageView];
    }
}


-(ILPDFDocument *)mergePDFToMakeFInalPDF
{
    ILPDFDocument *document;
    NSMutableArray<ILPDFDocument*> *finalDocuments = [NSMutableArray array];
    
    if (self.pdfRenderingQueue.count == 1)
    {
        NSData *data;
        if ([self.pdfRenderingQueue[0] annotations] != nil)
        {
            data = [self.pdfViewController.PDFViewDocument staticPDFDataWithAnnotations:[self.pdfRenderingQueue[0] annotations]];
        }
        else
        {
            data = [self.pdfViewController.PDFViewDocument savedStaticPDFData];
        }
        
        document = [[ILPDFDocument alloc]initWithData:data];
    }
    else
    {
        for (int i = 0; i < self.pdfRenderingQueue.count; i++)
        {
            [finalDocuments addObject:[self.pdfRenderingQueue[i] targetPDFDocument]];
        }
        
        for (int i=0; i<finalDocuments.count ; i++)
        {
            if(finalDocuments.count>1)
            {
                NSData *data  = [finalDocuments[i] mergedDataWithDocumenWithAnnotations:finalDocuments[i+1] andannotations:[self.pdfRenderingQueue[i] annotations]];
                
                document = [[ILPDFDocument alloc]initWithData:data];
                finalDocuments[0] = document;
                [finalDocuments removeObjectAtIndex:1];
                i -- ;
            }
        }
    }
    return document;
}


#pragma - PDF Rendering Queue

- (PDFQueueElement *)findElementMatchingSourcePDFName:(NSString *)sourcePDFName andElementName:(NSString *)elementName
{
    for (PDFQueueElement *element in self.pdfRenderingQueue)
    {
        if ([element.sourcePdfName  isEqualToString:sourcePDFName] &&
            [element.elementName    isEqualToString:elementName])
        {
            return element;
        }
    }
    
    return nil;
}


- (PDFQueueElement *)findElementInAllPDFsMatchingTargetPDFName:(NSString *)targetPDFName
{
    for (PDFQueueElement *element in self.allPdfs)
    {
        if ([element.targetPdfName  isEqualToString:targetPDFName])
        {
            return element;
        }
    }
    
    return nil;
}


#pragma - printAlert

-(void)printAlert
{
    self.pdfViewController.navigationItem.rightBarButtonItems = nil;
    self.pdfViewController.navigationItem.leftBarButtonItem = nil;
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"PDF Ready"
                                                    message:@"Your final PDF is Ready"
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
    
}


#pragma - CombineImages

-(ILPDFDocument *)combineImages:(NSArray *)images andumberOfImagesPerPage:(NSUInteger )numberOfImagesPerPage andDocumentToAppend: (ILPDFDocument *)document
{
    self.combinedImagesArray = [[NSMutableArray alloc]init];
    CGFloat runningHeight = 0.0;
    CGFloat runningWidth = 0.0;
    
    if (images.count == 1 || numberOfImagesPerPage == 1)
    {
        document = [self createDocument:images withDocument:document];
    }
    
    else if (numberOfImagesPerPage > 1)
    {
        int index = 1;
        CGSize finalSize = CGSizeMake(2736, 3648);
        UIGraphicsBeginImageContext(finalSize);
        NSMutableArray *resizedImages = [NSMutableArray array];
        
        for (UIImage *image in images)
        {
            int requiredHeight = image.size.height;
            int requiredWidth = image.size.width;
            if(image.size.height > finalSize.height/2 && numberOfImagesPerPage > 1 )
            {
                requiredHeight = image.size.height - (image.size.height - finalSize.height/2);
            }
            if (image.size.width > image.size.height && numberOfImagesPerPage > 2 )
            {
                requiredHeight = image.size.height - 1400;
                requiredWidth = image.size.width - image.size.width / 2.55 ;
            }
             if (image.size.width < image.size.height && numberOfImagesPerPage > 2 )
            {
                requiredWidth = image.size.width - 900;
            }
            
            if (image.size.width > image.size.height && numberOfImagesPerPage == 2 )
            {
//                requiredHeight = image.size.height - 1100;
//                requiredWidth = image.size.width - 900;
            }
            else if (image.size.width < image.size.height && numberOfImagesPerPage == 2 )
            {
                requiredWidth = image.size.width - image.size.width / 2.7;
            }
            
            if (image.size.width > finalSize.width)
            {
                requiredWidth = requiredWidth - image.size.width / 5;
            }
            
            UIImage *resizedImage = [self imageWithImage:image convertToSize:CGSizeMake(requiredWidth , requiredHeight - 60)];
            [resizedImages addObject:resizedImage];
        }
        
        NSArray *borderedImages = [self borderImages:resizedImages];
        
        [self populateCaptions];
        
        for (UIImage *image in borderedImages)
        {
            if (numberOfImagesPerPage == 2)
            {
                runningWidth = ((finalSize.width)/2) - (image.size.width)/2;
            }
            
            CGRect imageRect = CGRectMake(runningWidth, runningHeight, image.size.width , image.size.height );
            [image drawInRect:imageRect];
            
            if(numberOfImagesPerPage > 2)
            {
                if(runningWidth > image.size.width)
                {
                    runningHeight += finalSize.height/2 + 40;
                    runningWidth = 0.0;
                }
                else
                {
                    runningWidth += image.size.width + 40 ;
                }
            }
            else
            {
                runningWidth =  (finalSize.width/2)-(image.size.width/2) ;
                runningHeight += finalSize.height/2 + 40;
            }
            
            if ((index % numberOfImagesPerPage) == 0 || index == images.count )
            {
                self.combinedImage = UIGraphicsGetImageFromCurrentImageContext();
                if (self.combinedImage != nil)
                {
                    runningHeight = 0.0;
                    [self.combinedImagesArray addObject:self.combinedImage];
                }
                
                UIGraphicsBeginImageContext(finalSize);
            }
            index = index + 1;
        }
        document = [self createDocument:self.combinedImagesArray withDocument:document];
    }
    return document;
}


- (void)populateCaptions
{
    // Subclasses should override this method
    return;
}


-(ILPDFDocument *)combineImages:(NSArray *)images andDocumentToAppend: (ILPDFDocument *)document
{
    self.combinedSignatureArray = [NSMutableArray array];
    CGSize finalSize = CGSizeMake(3648, 4800);
    
    UIGraphicsBeginImageContext(finalSize);
    
    CGFloat runningHeight = 0.0;
    CGFloat runningWidth = 0.0;
    NSMutableArray *resizedImages = [NSMutableArray array];
    int index = 1;
    for (UIImage *image in images)
    {
        UIImage *resizedImage = [self imageWithImage:image convertToSize:CGSizeMake(4800.0f, 435.0f)];
        [resizedImages addObject:resizedImage];
    }
    
    for (UIImage *image in resizedImages)
    {
        CGRect imageRect = CGRectMake(50, runningHeight, image.size.width , image.size.height);
        [image drawInRect:imageRect];
        runningHeight += image.size.height + 300;
        runningWidth = 0.0;
        if (index == 6 || index == images.count )
        {
            self.combinedSignatureImage = UIGraphicsGetImageFromCurrentImageContext();
            if (self.combinedSignatureImage != nil)
            {
                [self.combinedSignatureArray addObject:self.combinedSignatureImage];
                runningHeight = 0.0;
            }
            
            UIGraphicsBeginImageContext(finalSize);
        }
        index ++;
    }
    
    document = [self createDocument:self.combinedSignatureArray withDocument:document];
    
    return  document;
}


#pragma Mark - Creating documet by Combining images

-(ILPDFDocument *)createDocument : (NSArray *)combinedImagesArray withDocument: (ILPDFDocument *)document
{
    CGSize pageSize = CGSizeMake(612, 792);
    CGRect rect = CGRectMake(50, 50, 512, 692);
    
    for (UIImage *image in combinedImagesArray)
    {
        NSData *data  = [PDFImageConverter convertImageToPDF:image withResolution:300 maxBoundsRect:rect pageSize:pageSize];
        ILPDFDocument *imagePDF = [[ILPDFDocument alloc]initWithData:data];
        NSData *dataWithImage  = [document mergedDataWithDocument:imagePDF];
        document = [[ILPDFDocument alloc]initWithData:dataWithImage];
    }
    return document;
}


- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}


- (NSArray *)borderImages:(NSArray *)images
{
    NSMutableArray *borderedImages = [NSMutableArray array];
    for (UIImage *image in images)
    {
        UIImage *borderedImage = [image addBorder];
        [borderedImages addObject:borderedImage];
    }
    
    return [borderedImages copy];
}


#pragma Mark - Addign text to Image

-(UIImage*) drawText:(NSString*) text inImage:(UIImage*) image atPoint:(CGPoint) point
{
    
    UIFont *font = [UIFont  systemFontOfSize:60];
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.alignment = NSTextAlignmentLeft;
    NSDictionary *attribute = [NSDictionary dictionaryWithObject:style forKey:NSParagraphStyleAttributeName];
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(450,30,2000,300)];
    
    CGRect rect = CGRectMake(point.x, point.y, image.size.width + 100, image.size.height+30);
    [[UIColor blackColor] set];
    [text drawInRect:CGRectIntegral(rect) withAttributes:@{NSFontAttributeName:font, NSFontAttributeName: attribute}];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


#pragma mark - AnnotatedDocuments Delegate

- (void)setAnnotationElement:(PDFAnnotations *)annotations
{
    self.currentPDFQueueElement = self.pdfRenderingQueue[self.currentIndex];
    
    if(self.currentPDFQueueElement.annotations == nil || self.currentPDFQueueElement.annotations.count == 0 )
    {
        self.currentPDFQueueElement.annotations = [NSMutableArray array];
    }
    
    if(self.currentPDFQueueElement.targetDocument.annotations == nil || self.currentPDFQueueElement.targetDocument.annotations.count == 0 )
    {
        self.currentPDFQueueElement.targetDocument.annotations = [NSMutableArray array];
    }
    
    if (![self.currentPDFQueueElement.annotations containsObject:annotations])
    {
        [self.currentPDFQueueElement.annotations addObject:annotations];
        [self.currentPDFQueueElement.targetDocument.annotations addObject:annotations];
    }
}


-(void)updateAnnotationforQueuElement:(NSArray *)annotations
{
     self.currentPDFQueueElement = self.pdfRenderingQueue[self.currentIndex];
     self.currentPDFQueueElement.annotations = [NSMutableArray arrayWithArray:annotations];
    self.currentPDFQueueElement.targetDocument.annotations = [NSMutableArray arrayWithArray:annotations];
}


-(void)doneButtonPressed
{
    [self renderAnnotations];
}


#pragma WKNavigationDelegate

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
//    [self.pdfViewController.pdfView webView:webView didFinishNavigation:navigation];
    
    [self renderAnnotations];
}

@end

#pragma - PdfQueueElement

@implementation PDFQueueElement

- (instancetype)initWithSourcePdfName:(NSString *)sourcePdfName elementName:(NSString *)elementName targetPdfName:(NSString *)targetPdfName andTargetDocument:(ILPDFDocument *)targetDocument
{
    self = [super init];
    
    if (self)
    {
        self.sourcePdfName = sourcePdfName;
        self.elementName = elementName;
        self.targetPdfName = targetPdfName;
        self.targetDocument = targetDocument;
        self.replaceWithTheseSignatureAnnotation = [NSMutableArray array];
    }
    return self;
}


- (instancetype)initWithPdfQueueElement:(PDFQueueElement *)pdfQueueElement
{
    PDFQueueElement *element = [[PDFQueueElement alloc]
                                initWithSourcePdfName:pdfQueueElement.sourcePdfName
                                elementName:pdfQueueElement.elementName
                                targetPdfName:pdfQueueElement.targetPdfName
                                andTargetDocument:pdfQueueElement.targetDocument];
    
    return element;
}


- (instancetype)initWithPdfName:(NSString *)pdfName andOriginalDocumentData:(NSData *)originalDocumentData
{
    self = [self initWithSourcePdfName:nil elementName:nil targetPdfName:pdfName andTargetDocument:nil];
    if (self)
    {
        self.originalDocumentData = originalDocumentData;
    }
    
    return self;
}


- (BOOL)isEqual:(id)object
{
    BOOL isEqual = NO;
    
    if ([object isKindOfClass:[PDFQueueElement class]])
    {
        PDFQueueElement *comparedElement = (PDFQueueElement *)object;
        
        if (self.sourcePdfName == nil && comparedElement.sourcePdfName == nil)
        {
            if ([self.elementName isEqualToString:comparedElement.elementName])
            {
                return YES;
            }
            else
            {
                return NO;
            }
        }
        
        if ([comparedElement.sourcePdfName  isEqualToString:self.sourcePdfName] &&
            [comparedElement.elementName    isEqualToString:self.elementName])
        {
            isEqual = YES;
        }
    }
    return isEqual;
}


#pragma Properties

- (ILPDFDocument *)targetDocument
{
    if (_targetDocument == nil)
    {
        if (self.originalDocumentData != nil)
        {
            _targetDocument = [[ILPDFDocument alloc] initWithData:self.originalDocumentData];
//             _targetDocument = [[PDFDocument alloc] initWithData:self.originalDocumentData];
        }
    }
    
    return _targetDocument;
}




@end
