//
//  CIPDFViewController.h
//  PDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 21/6/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CIPDFController.h"

@interface CIPDFViewController : UIViewController
@property (strong,nonatomic) NSMutableArray *imagesTaken;
@property (strong,nonatomic) NSMutableArray *signaturesCollected;
@property (nonatomic, strong) NSArray *finalSignatures;
@property (nonatomic, strong) NSArray *finalImages;
@property (nonatomic, strong) NSMutableArray *signaturesWithNameAndDate;
@property (nonatomic, assign) int numberOfImagesPerPage;
@property (nonatomic, strong) NSMutableArray *annotatedDocuments;
@property (nonatomic, assign) NSInteger strokeWidth;
@property (nonatomic, strong) ILPDFViewController *pdfVC;

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;

- (NSArray *)generateAllPDFs;
-(CGRect)currentPDFPageFrame: (NSUInteger)pageNumber;

@end
