// ILPDFViewController.m
//
// Copyright (c) 2017 Derek Blair
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "ILPDFKit.h"
#import "ILPDFFormContainer.h"
#import "UIView+CIPDFView.h"
#import "ILPDFSignatureController.h"
#import "ILPDFFormSignatureField.h"
#import "CIPDFController.h"
#import "SignPage.h"
#import "PDFSignatureAnnotation.h"
#import "PDFSignatureAnnotation.h"

@interface ILPDFViewController(Private) <UITextFieldDelegate, NSURLConnectionDelegate,ILPDFSignatureControllerDelegate, PDFViewDelegate,PDFDocumentDelegate>
- (void)loadPDFView;
@property (nonatomic, strong) PDFView *pdfView;

@property (nonatomic, strong) NSMutableArray *signatureAnnotationsArray;

@end


@implementation ILPDFViewController
CGFloat animatedDistance;
CGFloat keyboardHeight;
ILPDFSignatureController *signatureController;
ILPDFFormSignatureField *signatureField;
PDFAnnotation *signatureAnnotation;
UIScrollView *scrollV;
ILPDFWidgetAnnotationView *_formUIElement;

#pragma mark - UIViewController

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        [self loadPDFView];
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Listen for keyboard appearances and disappearances
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChangeFrame:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showSignatureViewController:)
                                                 name:@"SignatureNotification"
                                               object:nil];
}


- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


#pragma mark - Signature Controller Delegate

- (void) signedWithImage:(UIImage*) signatureImage forObject:(id)object
{
    DrawSignature *imageStamp = (DrawSignature *)object;
    imageStamp.image = signatureImage;
    
    NSMutableData *pageData = [NSMutableData data];
    UIGraphicsBeginPDFContextToData(pageData, CGRectZero , nil);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    [self.pdfView.currentPage addAnnotation:imageStamp];
//    [self.pdfView setNeedsDisplay];
}


#pragma mark - ILPDFViewController

#pragma mark - Setting the Document
- (void)setDocument:(ILPDFDocument *)document{
    _document = document;
    [self loadPDFView];
}


#pragma mark - Relaoding the Document
- (void)reload
{
    [_document refresh];
    [self loadPDFView];
}


// Override to customize constraints.
- (void)applyConstraintsToPDFView
{
    [self.pdfView pinToSuperview:UIEdgeInsetsZero guide:self.view.layoutMarginsGuide];
}


#pragma mark - Private

- (void)loadPDFView
{
    if(self.document.replaceWithTheseSignatureAnnotations == nil)
    {
        self.document.replaceWithTheseSignatureAnnotations = [NSMutableArray array];
    }
    if(self.view.subviews != nil)
    {
        [self.pdfView removeFromSuperview];
    }
    _pdfView = [[PDFView alloc] initWithFrame:self.view.frame];
    self.signedAnnotationsArray = [NSMutableArray array];
    self.pdfView.backgroundColor = [UIColor lightGrayColor];
   
    self.pdfView.delegate = self;
  
    PDFDocument *doc = [[PDFDocument alloc]initWithData:_document.documentData];
    self.pdfView.document = doc;
    NSMutableArray *drawSignatures;
    if (@available(iOS 11.0, *)) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PDFViewAnnotationHitNotification:) name:PDFViewAnnotationHitNotification object:self.pdfView];
    } else {
        // Fallback on earlier versions
    }

    for (PDFPage *page in self.pdfView.visiblePages)
    {
        NSMutableArray *deleteTheseAnnotations = [NSMutableArray array];
        drawSignatures = [NSMutableArray array];

        for (PDFAnnotation *annot in page.annotations)
        {
            if([annot valueForAnnotationKey:@"/Opt"])
            {
                NSLog(@"annotation key value is %@",[annot annotationKeyValues]);
            }
           if([[annot valueForAnnotationKey:@"/T"] isEqualToString:@"Signature"])
           {
                [deleteTheseAnnotations addObject:annot];
           }
            
            if([annot.widgetFieldType isEqualToString: @"/Sig"])
            {
                [deleteTheseAnnotations addObject:annot];
                
                DrawSignature *drawSignature = [[DrawSignature alloc] initWithImage:nil bounds:annot.bounds withProperties:nil withPage:annot.page];

                [self.document.replaceWithTheseSignatureAnnotations addObject:drawSignature];
            }
        }

        for (PDFAnnotation *annotation in deleteTheseAnnotations)
        {
            [page removeAnnotation:annotation];
        }
        
        for (DrawSignature *signature in self.document.replaceWithTheseSignatureAnnotations)
        {
            NSString *annotaionPage = [signature valueForAnnotationKey:@"/Page"] ;
            
            NSUInteger pageNumberForAnnotation = [annotaionPage intValue];
            
            CGPDFPageRef pdfPageRef = page.pageRef;
            NSUInteger pageNumberForPdf = CGPDFPageGetPageNumber(pdfPageRef);
            
            if (pageNumberForAnnotation == pageNumberForPdf || page == signature.page )
            {
                [page addAnnotation:signature];
            }
        }
    }
  
    [self.view addSubview:self.pdfView];
    [self.pdfView pinToSuperview:UIEdgeInsetsZero guide:self.view.layoutMarginsGuide];
}


-(void)PDFViewAnnotationHitNotification:(NSNotification*)notification
{
    PDFAnnotation *annot = notification.userInfo[@"PDFAnnotationHit"];
    
    if ([annot isKindOfClass:[DrawSignature class]]  || [[annot valueForAnnotationKey:@"/T"] isEqualToString:@"Signature"])
    {
        ILPDFSignatureController *vc = [[ILPDFSignatureController alloc] initWithObject:annot];
        vc.expectedSignSize = annot.bounds.size;
        vc.delegate = self;

        UINavigationController *popOverNavigationController = [[UINavigationController alloc] initWithRootViewController:vc];
        [popOverNavigationController setModalPresentationStyle:UIModalPresentationFormSheet];
        [self.navigationController presentViewController:popOverNavigationController animated:YES completion:nil];
    }
}


- (void)PDFViewWillClickOnLink:(PDFView *)sender withURL:(NSURL *)url
{
//    self.pdfView.document.delegate = self;
//
//    ILPDFSignatureController *vc = [ILPDFSignatureController alloc];
//    NSString *urlString = [url absoluteString];
//    NSString *frameString = [urlString componentsSeparatedByString:@"_"][1];
//
//    UIButton *signatureButton = [[UIButton alloc]init];
//    signatureButton.frame = CGRectMake([[frameString componentsSeparatedByString:@","][0] doubleValue], [[frameString componentsSeparatedByString:@","][1] doubleValue], [[frameString componentsSeparatedByString:@","][2] doubleValue], [[frameString componentsSeparatedByString:@","][3] doubleValue]);
//
//    for (PDFAnnotation *annot in self.signatureAnnotationsArray)
//    {
//        if(annot.bounds.origin.x == signatureButton.frame.origin.x && annot.bounds.origin.y == signatureButton.frame.origin.y )
//        {
//            self.clickedSignatureAnnotation = annot;
////              [_signatureAnnotationArray removeObject:annot];
//        }
//    }
//    vc.expectedSignSize = signatureButton.frame.size;
//    vc.delegate = self;
//
//    UINavigationController *popOverNavigationController = [[UINavigationController alloc] initWithRootViewController:vc];
//    [popOverNavigationController setModalPresentationStyle:UIModalPresentationFormSheet];
//    [self.navigationController presentViewController:popOverNavigationController animated:YES completion:nil];

}


- (void)keyboardWillChangeFrame:(NSNotification *)notification
{
    UITextField *currentTextField = (UITextField *)[self.pdfView activePDFTextField];
    CGRect activeTextFieldFrame = currentTextField.frame;
    CGRect textFieldFrameInWindow = [self.view.window convertRect:activeTextFieldFrame fromView:currentTextField.superview];
    CGRect keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardHeight = keyboardRect.size.height;
    Boolean viewsOverlap = CGRectIntersectsRect(textFieldFrameInWindow, keyboardRect);
    keyboardHeight = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    double distancetoBottom = self.pdfView.frame.size.height - (textFieldFrameInWindow.origin.y) - textFieldFrameInWindow.size.height;
    double collapseDistance = keyboardRect.origin.y - distancetoBottom;
    //    scrollV = self.pdfView.pdfView.scrollView;
    
    if(viewsOverlap)
    {
        [UIView animateWithDuration:0.3f
                         animations:^{
                             if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
                                 
                                 UIEdgeInsets insets = UIEdgeInsetsMake(scrollV.contentInset.top, 0, collapseDistance+80, 0);
                                 scrollV.contentInset = insets;
                                 scrollV.scrollIndicatorInsets = insets;
                                 scrollV.contentOffset = CGPointMake(scrollV.contentOffset.x, scrollV.contentOffset.y+collapseDistance+80);
                             }
                             else
                             {
                                 UIEdgeInsets insets = UIEdgeInsetsMake(scrollV.contentInset.top, 0, collapseDistance-keyboardHeight+10, 0);
                                 scrollV.contentInset = insets;
                                 scrollV.scrollIndicatorInsets = insets;
                                 scrollV.contentOffset = CGPointMake(scrollV.contentOffset.x, scrollV.contentOffset.y+collapseDistance-keyboardHeight);
                             }
                             
                         }
                         completion:nil];
    }
}


- (void)keyboardDidHide: (NSNotification *) notif
{
    [UIView animateWithDuration:0.3f
                     animations:^{
                         UIEdgeInsets insets = UIEdgeInsetsMake(scrollV.contentInset.top, 0, 0, 0);
                         scrollV.contentInset = insets;
                         scrollV.scrollIndicatorInsets = insets;
                         scrollV.contentOffset = CGPointMake(scrollV.contentOffset.x, scrollV.contentOffset.y);
                         self.pdfView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
                     }
                     completion:nil];
}

@end
