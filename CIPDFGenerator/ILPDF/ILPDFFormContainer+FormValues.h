//
//  ILPDFFormContainer+FormValues.h
//  ILPDFKit
//
//  Created by Gagandeep Kaur Swaitch on 21/5/18.
//

#import "ILPDFKit.h"
#import "ILPDFFormContainer.h"

@interface ILPDFFormContainer (FormValues)


-(NSDictionary*) getFormValues;
-(NSDictionary*) getPDFNames;
-(NSArray*) getRequiredFields;
- (NSDictionary *)getSelectedButtons;

@end
