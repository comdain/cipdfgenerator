//
//  ILPDFSignatureController.m
//  Pods
//
//  Created by Yuriy on 28/08/16.
//
//

#import "ILPDFSignatureController.h"
#import "ILPDFSignatureEditingView.h"
#import "ILPDFFormSignatureField.h"
#import "ILPDFForm.h"

@interface ILPDFSignatureController ()
@property (strong, nonatomic) IBOutlet ILPDFSignatureEditingView *signatureView;
@property (strong, nonatomic) id object;

@end

@implementation ILPDFSignatureController

- (instancetype)initWithObject:(id)object
{
    self = [super init];
    if(self)
    {
        self.object = object;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    if (self.signatureImage != nil)
    {
        self.signatureView.incrementalImage = self.signatureImage;
    }
    
    self.view.backgroundColor = [UIColor clearColor];
    self.preferredContentSize=CGSizeMake(600, 300);
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Done"
                                   style:UIBarButtonItemStyleDone
                                   target:self
                                   action:@selector(signatureAction:)];
    
    UIBarButtonItem *clearButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Clear"
                                   style:UIBarButtonItemStyleDone
                                   target:self
                                   action:@selector(clearAction:)];
    self.navigationItem.rightBarButtonItem = doneButton;
    self.navigationItem.leftBarButtonItem = clearButton;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)clearAction:(UIButton *)sender
{
    
    [self.signatureView clearSignature];
    
}

- (IBAction)signatureAction:(UIButton *)sender
{
    self.signatureImage = [self.signatureView createImageFromSignWithMaxWidth:self.expectedSignSize.width andMaxHeight:self.expectedSignSize.height];
    
    if(self.expectedSignSize.width == 0)
    {
        self.signatureImage = [self.signatureView createImageFromSignWithMaxWidth:1000 andMaxHeight:260];
    }
    
    [self.delegate signedWithImage:self.signatureImage forObject:self.object];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
