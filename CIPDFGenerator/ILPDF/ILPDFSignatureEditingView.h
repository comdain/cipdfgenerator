//
//  ILPDFSignatureEditingView.h
//  Pods
//
//  Created by Yuriy on 28/08/16.
//
//

#import <UIKit/UIKit.h>

@interface ILPDFSignatureEditingView : UIView

//need to add size
- (UIImage*) createImageFromSignWithMaxWidth:(CGFloat) width andMaxHeight:(CGFloat) height;
- (UIImage *)imageWithImage:(UIImage *)image scaledToMaxWidth:(CGFloat)width maxHeight:(CGFloat)height;
- (void) clearSignature;
@property (nonatomic, strong) UIImage *incrementalImage;
@end
