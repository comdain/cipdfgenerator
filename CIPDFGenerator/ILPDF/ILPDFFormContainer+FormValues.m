//
//  ILPDFFormContainer+FormValues.m
//  ILPDFKit
//
//  Created by Gagandeep Kaur Swaitch on 21/5/18.
//

#import "ILPDFFormContainer+FormValues.h"

@implementation ILPDFFormContainer (FormValues)

-(NSDictionary *)getFormValues
{
    NSMutableDictionary *formValues;
    formValues = [NSMutableDictionary dictionary];
    
    for (ILPDFForm *form in self)
    {
        if ([form.name  isEqual: @"Symmetrical Load 1"])
        {
             [formValues setObject:form.value forKey:form.name];
        }
        if(form.value != nil)
        {
            [formValues setObject:form.value forKey:form.name];
        }
    }
    
    return [formValues copy];
}


-(NSArray *) getRequiredFields
{
    NSMutableArray *fieldsToBeFilled = [NSMutableArray array];
    NSMutableArray *sectionFieldsToBeFilled = [NSMutableArray array];
    NSMutableArray *groupedSectionArray ;
    NSMutableDictionary *sectionDictionary = [NSMutableDictionary dictionary];
    for (ILPDFForm *form in self)
    {
        NSNumber *n = [form.dictionary objectForKey:@"Ff"];
        
//        NSArray *components = [form.name componentsSeparatedByString:@"#"];
//        NSString *sectionName;
//        if (components.count > 1)
//        {
//           sectionName = components[1];
//        }
//
//        if(sectionName)
//        {
//            if(![sectionDictionary objectForKey:sectionName])
//            {
//                groupedSectionArray = [NSMutableArray array];
//            }
//            if(![groupedSectionArray containsObject:form])
//            {
//                 [groupedSectionArray addObject:form];
//            }
//
//            [sectionDictionary setObject:groupedSectionArray forKey:sectionName];
//            NSLog(@"Section dictionary is %@", sectionDictionary);
//        }
        
        if([n isEqualToNumber:[NSNumber numberWithInt:2]])
        {
            if((form.value == nil) && (form.formType != ILPDFFormTypeSignature))
            {
                [fieldsToBeFilled addObject:form.name];
            }
            if ((form.formType == ILPDFFormTypeSignature) && (form.signatureImage == nil))
            {
                [fieldsToBeFilled addObject:form.name];
            }
        }
    }
    
//    for (id key in sectionDictionary)
//    {
//        NSArray *values = [sectionDictionary objectForKey:key];
//
//        for(ILPDFForm  *form in values)
//        {
//            if ([form.value isEqualToString:@"0"])
//            {
//                sectionFieldsToBeFilled = [NSMutableArray array];
//                break;
//            }
//            else
//            {
//                if(![sectionFieldsToBeFilled containsObject:form.name])
//                {
//                    [sectionFieldsToBeFilled addObject:form.name];
//                }
//            }
//        }
//        if(sectionFieldsToBeFilled.count != 0)
//        {
//            for(NSString *formName in sectionFieldsToBeFilled)
//            {
//                [fieldsToBeFilled addObject:formName];
//            }
//        }
//    }
    return [fieldsToBeFilled copy];
}


-(NSDictionary *)getPDFNames
{
    NSMutableDictionary *pdfNames=[NSMutableDictionary dictionary];
    
    ILPDFForm *form = form;
    
    for (form in self)
    {
        NSString *pdfName = @"";
        NSData *pdfNameData = nil;

        if (form.formType == ILPDFFormTypeButton)
        {
            NSDictionary *aa = form.dictionary[@"AA"];

            for (NSString *key in aa)
            {
                pdfNameData = aa[key][@"F"][@"F"];
                
                if (pdfNameData != nil)
                {
                    break;
                }
            }

            if (pdfNameData != nil)
            {
                pdfName = [[pdfNameData utf8TextString] lastPathComponent];
            }
            
            [pdfNames setValue:pdfName forKey: [NSString stringWithFormat:@"%@_%@",form.name,form.exportValue]];
        }
    }
    return [pdfNames copy];
}


- (NSDictionary *)getSelectedButtons
{
    NSMutableDictionary *selectedButtons=[NSMutableDictionary dictionary];
    ILPDFForm *form = form;
    
    for (form in self)
    {
        NSString *pdfName = @"";
        NSData *pdfNameData = nil;
        
        if (form.formType == ILPDFFormTypeButton && (form.exportValue != nil && form.exportValue == form.value))
        {
//            for (NSString *key in aa)
//            {
//                pdfNameData = aa[key][@"F"][@"F"];
//
//                if (pdfNameData != nil)
//                {
//                    break;
//                }
//            }
            
//            if (pdfNameData != nil)
//            {
//                pdfName = [[pdfNameData utf8TextString] lastPathComponent];
//            }
            
            [selectedButtons setValue:form.name forKey:[NSString stringWithFormat:@"%@_%@",form.name,form.exportValue]];
        }
    }
    
    return [selectedButtons copy];
}


@end
