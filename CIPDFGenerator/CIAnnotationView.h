//
//  AnnotationView.h
//  CIPDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 6/8/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILPDFKit.h"
#import "CIAnnotationZoomView.h"
#import "CIPDFViewController.h"
#import "PDFAnnotations.h"


@protocol AnnotatedDocumentDelegate;

@interface CIAnnotationView : UIScrollView

@property (nonatomic, strong) NSMutableDictionary *documentWithAnnotation;
@property (nonatomic, strong) NSMutableArray *annotatedDocuments;
@property (nonatomic, weak)  id <AnnotatedDocumentDelegate> annotatedDocumentDelegate;
@property (nonatomic, strong) NSMutableArray *annotations;
@property (nonatomic, strong) UIView *viewContainer;

- (id) initWithPDFView:(PDFView *)pdfView andpdfDocument:(ILPDFDocument *)pdfDocument andAnnottaions:(NSArray *)annotations;


- (void) doAnnotation;
- (void) undoPressed;
- (void) erasePressed;
- (void) doneButtonPressed: (ILPDFDocument *)pdfDcoument andpdfView: (UIView *)pdfView;
- (void) setZoomScalesAndContent : (ILPDFView *)pdfView;

- (void)setStrokeColor:(UIColor *)strokeColor;
- (void)setStrokeWidth : (NSInteger )strokeWidth;
@end

@protocol AnnotatedDocumentDelegate <NSObject>

@required

-(void)setAnnotationElement: (PDFAnnotations *)annotations;

-(void)updateAnnotationforQueuElement: (NSArray *)annotations;

@end

