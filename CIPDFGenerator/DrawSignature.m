//
//  drawSignature.m
//  CIPDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 3/12/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import "DrawSignature.h"

@implementation DrawSignature


-(instancetype)initWithImage:(UIImage *)signatureImage bounds:(CGRect)bounds withProperties:AnyHashable withPage:(PDFPage *)page
{
    self = [super initWithBounds:bounds forType:PDFAnnotationSubtypeWidget withProperties:AnyHashable];
    
    if (self)
    {
        self.widgetFieldType = PDFAnnotationWidgetSubtypeButton;
        self.widgetControlType = kPDFWidgetPushButtonControl;
        
        CGPDFPageRef pageRef = page.pageRef;
        NSUInteger pageNumberForAnnotation = CGPDFPageGetPageNumber(pageRef);
        
        self.caption = @"Sign";
        [self setValue:@"Signature" forAnnotationKey:@"/T"];
        [self setValue:[NSString stringWithFormat:@"%lu", (unsigned long)pageNumberForAnnotation] forAnnotationKey:@"/Page"];
//        [self setValue:[NSString stringWithFormat:@"%@", page] forAnnotationKey:@"/Page"];
        
        self.page = page;
        self.image = signatureImage;
        self.shouldPrint = YES;
        self.shouldDisplay = YES;
    }

    return self;
}


- (void)drawWithBox:(PDFDisplayBox)box inContext:(nonnull CGContextRef)context
{
    if (self.image == nil)
    {
        [super drawWithBox:box inContext:context];
    }
    else
    {
        struct CGImage *cgImage = self.image.CGImage;
        if(!cgImage)
            return;
        
        UIGraphicsPushContext(context);
        CGContextSaveGState(context);
        
        CGContextDrawImage(context, self.bounds, cgImage);
        
        CGContextRestoreGState(context);
        UIGraphicsPopContext();
    }
}


@end
