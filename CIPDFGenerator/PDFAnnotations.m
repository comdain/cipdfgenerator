//
//  PDFAnnotations.m
//  CIPDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 16/8/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import "PDFAnnotations.h"
#import "ILPDFKit.h"

@implementation PDFAnnotations


-(instancetype)initWithImage: (NSArray *)annotationImages andZoomView: (CIAnnotationZoomView *)zoomView andPageNumber:(NSInteger )pageNumber andAllPossibleZoomViews: (NSArray *)allZoomViews
{
    self = [super init];
    if (self)
    {
        self.zoomView = zoomView;
        self.incrementImages = annotationImages;
        self.pageNumber = pageNumber;
        self.allPossileZoomViews = allZoomViews;
    }
    return self;
}
@end
