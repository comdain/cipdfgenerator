//
//  PDFSignatureAnnotation.h
//  CIPDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 10/12/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import "ILPDFFormContainer.h"
#import <PDFKit/PDFKit.h>

@class DrawSignature;

NS_ASSUME_NONNULL_BEGIN

@interface PDFSignatureAnnotation : ILPDFForm

- (instancetype)initWithDrawSignature:(DrawSignature *)drawSignature;

@end

NS_ASSUME_NONNULL_END
