//
//  CIAnnotController.m
//  CIPDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 7/8/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import "CIAnnotationZoomView.h"
#import "CIPDFViewController.h"
#import "CIAnnotationView.h"
#import "PDFAnnotations.h"

@interface CIAnnotationZoomView ()

@property (nonatomic, strong) UIBezierPath *path;
@property (nonatomic, strong) UIImage *incrementalImage;

@property (nonatomic, strong) ILPDFDocument *pdfDocument;
@property (nonatomic, strong) CIPDFViewController *pdfVC;
@property (nonatomic) CGPoint pointInPDFView;
@property (nonatomic) CGRect frameToAnnotate;

@property (nonatomic, strong) NSMutableArray *annotationsArray;
@property (nonatomic, strong) PDFAnnotations *annotations;
@property (nonatomic, strong) CIAnnotationView *annotationView;
@end


@implementation CIAnnotationZoomView
{
    uint ctr;
    CGPoint pts[5];
    CGRect lastAnnotPosition;
    NSSet *lasttouch;
}


- (id)initWithFrame:(CGRect)frame andPDFView:(ILPDFView *)pdfView andAnnotationView: (UIView *)annotationView andAnnotatedImage: (NSArray *)lastAnnotatedImage
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setMultipleTouchEnabled:NO];
        
        self.path = [UIBezierPath bezierPath];
        [self.path setLineWidth:self.strokeWidth];
        self.annotationView = annotationView;
        self.pdfView = pdfView;
        
        if(self.incrementalImages == nil)
        {
            self.incrementalImages = [NSMutableArray array];
        }
        
    }
    self.isErase = NO;
    self.annotationsArray = [NSMutableArray array];
    return self;
}


- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    if(self.isErase)
    {
        CGContextSetBlendMode(context, kCGBlendModeClear);
    }
    else
    {
        CGContextSetStrokeColorWithColor(context, [self.strokeColor CGColor]);
    }
    
    [[self.incrementalImages lastObject] drawInRect:rect];

    [self.path setLineWidth:self.strokeWidth];
    
    [self.path stroke];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UIView* superView = self.pdfView;
    while (superView != nil)
    {
        if ([superView isKindOfClass:[UIScrollView class]])
        {
            UIScrollView* superScroll = (UIScrollView*)superView;
            superScroll.scrollEnabled = NO;
        }
        
        superView = superView.superview;
    }
    ctr = 0;
    UITouch *touch = [touches anyObject];
    pts[0] = [touch locationInView:self];
    
    self.pointInPDFView = [touch locationInView:self.annotationView.viewContainer];
    
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint p = [touch locationInView:self];
    ctr++;
    pts[ctr] = p;
    if (ctr == 4)
    {
        pts[3] = CGPointMake((pts[2].x + pts[4].x)/2.0, (pts[2].y + pts[4].y)/2.0); // move the endpoint to the middle of the line joining the second control point of the first Bezier segment and the first control point of the second Bezier segment
        
        [self.path moveToPoint:pts[0]];
        [self.path addCurveToPoint:pts[3] controlPoint1:pts[1] controlPoint2:pts[2]];
        
        [self setNeedsDisplay];
        // replace points and get ready to handle the next segment
        pts[0] = pts[3];
        pts[1] = pts[4];
        ctr = 1;
    }
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSUInteger numTaps = [[touches anyObject] tapCount];
    if (numTaps == 0)
    {
        UIView* superView = self.superview;
        //    UIView* superView = self.pdfView;
        while (superView != nil)
        {
            if ([superView isKindOfClass:[UIScrollView class]])
            {
                UIScrollView* superScroll = (UIScrollView*)superView;
                superScroll.scrollEnabled = YES;
            }
            superView = superView.superview;
        }
        
        [self drawBitmap];
        [self setNeedsDisplay];
        [self.path removeAllPoints];
        ctr = 0;
        
        CGPoint location = [[touches anyObject] locationInView:self];
        CGRect fingerRect = CGRectMake(location.x-5, location.y-5, 10, 10);
        lastAnnotPosition = fingerRect;
        lasttouch = touches;
        
        NSInteger currentPageNumber = [self findPage];
        
        [self.annotDelegate getView:self andIncrementalImage:self.incrementalImages  andPageNumber:currentPageNumber ];
        
    }
}


- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesEnded:touches withEvent:event];
}


- (void)drawBitmap
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 0.0);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
  
   
    CGContextSetStrokeColorWithColor(context, [self.strokeColor CGColor]);
    
    if(self.isErase)
    {
        CGContextSetBlendMode(context, kCGBlendModeClear);
    }
    
    [[self.incrementalImages lastObject] drawAtPoint:CGPointZero];

    [self.path stroke];
   
    self.incrementalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    [self.incrementalImages addObject:self.incrementalImage];
    
}


- (NSInteger )findPage
{
//    NSUInteger pageNumber = [self.pdfView pageNumberAtPoint:self.pointInPDFView];
    CGPDFPageRef pageRef = self.pdfView.currentPage.pageRef;
    NSUInteger pageNumber = CGPDFPageGetPageNumber(pageRef);
    return pageNumber;
}


-(BOOL )getLastAnnotatedVie:(CGRect)viewFrame andView:(CIAnnotationZoomView *)view
{
    CGPoint location = [[lasttouch anyObject] locationInView:view];
    CGRect fingerRect = CGRectMake(location.x-5, location.y-5, 10, 10);
    lastAnnotPosition = fingerRect;
    
    if(CGRectIntersectsRect(lastAnnotPosition, viewFrame))
    {
        return true;
    }
    return false;
}


-(void)eraseAnnotation
{
    self.isErase = YES;
}


@end
