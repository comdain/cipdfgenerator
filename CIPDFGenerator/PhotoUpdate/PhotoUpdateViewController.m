//
//  PhotoUpdateViewController.m
//  pdfForm
//
//  Created by Gagandeep Kaur Swaitch on 29/5/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import "PhotoUpdateViewController.h"
#import "CLImageEditor.h"

@interface PhotoCopy: NSObject

@property (nonatomic, strong) id<IPhoto> photo;
@property UIImage *image;
@property NSString *caption;
@property NSString *assetNumber;


@end

@implementation PhotoCopy


@end

@interface PhotoUpdateViewController () <UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate,CLImageEditorDelegate>

@property (nonatomic, strong) PhotoCopy *photoCopy;

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@property (weak, nonatomic) IBOutlet UITextView *captionTextView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UITextField *assetNumberTextField;
@property (weak, nonatomic) IBOutlet UILabel *assetNumberLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (weak, nonatomic) IBOutlet UILabel *captionLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cameraItemBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editImageBarItem;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UINavigationBar *naviagtionBar;


@property (nonatomic) CGFloat bottomConstraintDefault;
@property (nonatomic, strong) UIAlertController *actionAlertController;

@property (nonatomic, strong) NSString *imageType;
@property (nonatomic, strong) NSString *listName;
@property (nonatomic, strong) NSString *fieldTitle;
@property (nonatomic, strong, readonly) NSManagedObjectContext *context;
@property (nonatomic, strong) ILPDFViewController *pdfView;


- (IBAction)editItemPressed:(UIBarButtonItem *)sender;
- (IBAction)imageTypeSelected:(UITapGestureRecognizer *)sender;
- (IBAction)deleteButtonPressed:(UIBarButtonItem *)sender;

@end

@implementation PhotoUpdateViewController

- (instancetype)initWithPhoto:(UIImage*)photo andProcessBlock:(void (^)(ILPDFDocument *annotatedPDF, UIImage *image,NSString *assetNumber, NSString *caption))imageProcessed;
{
    self = [super init];
    
    if (self)
    {
        self.image = photo;
        self.imagedProcessed =  imageProcessed;
    }
    
    return self;
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)disableViewController
{
    [self.cameraItemBar setEnabled:NO];
    [self.editImageBarItem setEnabled:NO];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    
    UINavigationBar* navbar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0,width-30, 50)];
    
    UINavigationItem* navItem = [[UINavigationItem alloc] initWithTitle:@"Edit Photo"];
    // [navbar setBarTintColor:[UIColor lightGrayColor]];
    UIBarButtonItem* cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelButtonPressed)];
    navItem.leftBarButtonItem = cancelBtn;
    
    UIBarButtonItem* doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed)];
    navItem.rightBarButtonItem = doneBtn;
    
    [navbar setItems:@[navItem]];
    [self.view addSubview:navbar];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed)];
  
    self.preferredContentSize = CGSizeMake(width-40, height-50);
    UIImage *originalImage = self.image;
    
    UIImage *imageToDisplay =
    [UIImage imageWithCGImage:[originalImage CGImage]
                        scale:[originalImage scale]
                  orientation: UIImageOrientationUp];
    self.photoImageView.image = imageToDisplay;
    
    self.captionTextView.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
   
}

#pragma mark - Navigation buttons

- (void)cancelButtonPressed
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)doneButtonPressed
{
    CGSize pageSize = CGSizeMake(612, 792);
    CGRect imageBoundsRect = CGRectMake(50, 50, 512, 692);
    NSData *pdfData = [PDFImageConverter convertImageToPDF: self.image
                                            withResolution: 300 maxBoundsRect: imageBoundsRect pageSize: pageSize];
    
    ILPDFDocument *pdfDocument = [[ILPDFDocument alloc]initWithData:pdfData];
 
    if (self.imagedProcessed != nil)
    {
        self.imagedProcessed(pdfDocument, self.image, self.assetNumberTextField.text, self.captionTextView.text);
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];

}


#pragma mark - UITextViewDelegate Methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    self.title = newString;
    
    return YES;
}


- (void)keyboardWillShow:(NSNotification *)notification
{
    CGFloat height = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    
    self.bottomConstraint.constant = self.bottomConstraintDefault + height;
    
    [self animateFrameChange];
}


- (void)keyboardWillHide:(NSNotification *)notification
{
    self.bottomConstraint.constant = self.bottomConstraintDefault;
    
    [self animateFrameChange];
}


- (void)animateFrameChange
{
    [UIView animateWithDuration:0.5f animations:^{
        [self.view layoutIfNeeded];}
            completion:nil];
}


- (IBAction)editItemPressed:(UIBarButtonItem *)sender
{
    [self editImage];
}


- (void)editImage
{
    CLImageEditor *editor = [[CLImageEditor alloc] initWithImage:self.image];
    editor.delegate = self;
    
    for (CLImageToolInfo *toolInfo in editor.toolInfo.subtools)
    {
        toolInfo.available = NO;
    }
    
    CLImageToolInfo *tool;
    
    tool = [editor.toolInfo subToolInfoWithToolName:@"CLDrawTool" recursive:YES];
    tool.available = YES;
    tool.dockedNumber = 1;
    
    tool = [editor.toolInfo subToolInfoWithToolName:@"CLTextTool" recursive:YES];
    tool.available = YES;
    tool.dockedNumber = 2;
    
    tool = [editor.toolInfo subToolInfoWithToolName:@"CLStickerTool" recursive:YES];
    tool.available = NO;
    tool.dockedNumber = 3;
    
    tool = [editor.toolInfo subToolInfoWithToolName:@"CLClippingTool" recursive:YES];
    tool.available = NO;
    tool.dockedNumber = 4;
    
    tool = [editor.toolInfo subToolInfoWithToolName:@"CLAdjustmentTool" recursive:YES];
    tool.available = YES;
    tool.dockedNumber = 5;
    
    tool = [editor.toolInfo subToolInfoWithToolName:@"CLEmoticonTool" recursive:YES];
    tool.available = NO;
    tool.dockedNumber = 6;
    
    tool = [editor.toolInfo subToolInfoWithToolName:@"CLBlurTool" recursive:YES];
    tool.available = YES;
    tool.dockedNumber = 7;
    
    [self presentViewController:editor animated:YES completion:nil];
}


#pragma mark- CLImageEditor delegate
    
- (void)imageEditor:(CLImageEditor *)editor didFinishEdittingWithImage:(UIImage *)image
    {
        [editor dismissViewControllerAnimated:YES
                                   completion:nil];
        
        self.photo.image = image;
        ImageCollectionViewController *editedImageCollView = [[ImageCollectionViewController alloc]init];
        
        editedImageCollView.editedPhoto = image;
        
        [UIView transitionWithView:self.photoImageView
                          duration:1.0f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.photoImageView.image = image;
                        }
                        completion:nil];
        self.image = image;
    }
    
@end
