//
//  PDFSignatureAnnotation.m
//  CIPDFGenerator
//
//  Created by Gagandeep Kaur Swaitch on 10/12/18.
//  Copyright © 2018 Gagandeep Kaur Swaitch. All rights reserved.
//

#import "PDFSignatureAnnotation.h"
#import "DrawSignature.h"

@interface PDFSignatureAnnotation ()

@property (nonatomic, strong) DrawSignature *signatureAnnotation;

@end

@implementation PDFSignatureAnnotation


- (instancetype)initWithDrawSignature:(DrawSignature *)drawSignature
{
    self = [super init];
    
    if (self)
    {
        self.signatureAnnotation = drawSignature;
    }
    
    return self;
}


- (void)vectorRenderInPDFContext:(CGContextRef)ctx forRect:(CGRect)rect
{
    [self.signatureAnnotation drawWithBox:0 inContext:ctx];
}


- (NSUInteger)page
{
    CGPDFPageRef pageRef = self.signatureAnnotation.page.pageRef;
    NSUInteger pageNumber = CGPDFPageGetPageNumber(pageRef);
    
    return pageNumber;
}


@end
